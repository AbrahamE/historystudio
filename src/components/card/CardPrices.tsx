"use client";
interface CardPricesProps {
  title: string;
  currency: string;
  icon: JSX.Element;
  typePayment: string;
  typeCurrency: string;
}

export default function CardPrices({
  title,
  currency,
  icon,
  typePayment,
  typeCurrency,
}: CardPricesProps) {
  return (
    <div className="group ease-out duration-300 flex flex-col flex-1 p-5 justify-between rounded-lg bg-white hover:bg-black hover:ring-gray-900">
      <div className="flex flex-1 justify-between rounded-lg p-2">
        <p className="flex flex-col grow">
          <span className="grow group-hover:text-white font-semibold">
            {title}
          </span>
          <span className="grow group-hover:text-white">{currency}</span>
        </p>
        <section className="inline-flex items-center justify-center w-12 h-12 overflow-hidden bg-gray-100 rounded-full group-hover:bg-white dark:bg-gray-600">
          <span className="font-medium text-gray-600 group-hover:text-black dark:text-gray-300">
            {icon}
          </span>
        </section>
      </div>
      <p className="flex justify-between grow p-2">
        <span className="grow group-hover:text-white">{typePayment}</span>
        <span className="grow text-end group-hover:text-white">
          {typeCurrency}
        </span>
      </p>
    </div>
  );
}
