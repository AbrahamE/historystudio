"use client";
import { ReactNode } from "react"
import FormLayout from "@/components/form/FormLayout";
import ImgHover from "@/components/img/ImgHover";

export default function Page() {
  return (
    <ImgHover
        imgOne="/img/collins-lesulie.jpg"
        imgTwo="/img/eduardo-vaccari.jpg"
        type={2}
    />
  );
}
