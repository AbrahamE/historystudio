import BaceControllers from "@/controllers/BaceControllers";
import { AuthUser } from "@/types/User/AuthUser";

export function UserControllers() {
  const { client } = BaceControllers();

  async function create({ email, password }: AuthUser) {
    return client.auth.signUp({ email, password });
  }

  async function updateUser(
    { email, password }: AuthUser,
    data: Recort<string, string>
  ) {
    return client.auth.updateUser({ email, password, data });
  }

  async function inviteUserByEmail(email: string) {
    return client.auth.api.inviteUserByEmail(email);
  }

  async function resetPasswordForEmail(email: string) {
    return client.auth.resetPasswordForEmail(email);
  }

  async function getUser() {
    return client.auth.getUser();
  }

  return {
    getUser,
    create,
    updateUser,
    inviteUserByEmail,
    resetPasswordForEmail,
  };
}
