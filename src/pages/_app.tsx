// import { Inter } from "next/font/google";

// // If loading a variable font, you don't need to specify the font weight
// import { Montserrat } from "next/font/google";

// const montserrat = Montserrat({
//   weight: ["400", "700"],
//   style: ["normal", "italic"],
//   subsets: ["latin"],
//   display: "swap",
//   variable: "--font-montserrat",
// });

// interface AppProps {
//   Component: any;
//   pageProps: any;
// }

// export default function MyApp({ Component, pageProps }: AppProps) {
//   return (
//     <>
//       <style jsx global>{`
//         html {
//           font-family: ${montserrat.style.fontFamily} "Montserrat" !important;
//         }
//       `}</style>
//       <main className={montserrat.className}>
//         <Component {...pageProps} />
//       </main>
//     </>
//   );
// }
