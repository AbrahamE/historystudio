"use client";
import { redirect } from 'next/navigation'
import Image from "next/image";

export default function Page() {
  redirect('/auth')
  return (
    <div className="flex justify-center items-center gap-2 grow w-full h-screen">
       <Image
        alt="History Tattoo Logo"
        className="pb-10"
        src="/img/HISTORY-TATTOO-LOGO.png"
        width={200}
        height={200}
      />
    </div>
  );
}
