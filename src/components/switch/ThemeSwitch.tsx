"use client";
import { Switch, useSwitch, VisuallyHidden, SwitchProps } from "@nextui-org/react";
import { MoonIcon } from "@/components/icons/MoonIcon";
import { SunIcon } from "@/components/icons/SunIcon";

const ThemeSwitch = (props: SwitchProps) => {
    const {
        Component,
        slots,
        isSelected,
        getBaseProps,
        getInputProps,
        getWrapperProps
    } = useSwitch(props);

    return (
        <Component {...getBaseProps()}>
            <VisuallyHidden>
                <input {...getInputProps()} />
            </VisuallyHidden>
            <div
                {...getWrapperProps()}
                className={slots.wrapper({
                    class: [
                        "w-8 h-8 mr-0",
                        "flex items-center justify-center",
                        "rounded-lg bg-default-100 hover:bg-default-200",
                    ],
                })}
            >
                {isSelected ? <SunIcon /> : <MoonIcon />}
            </div>
        </Component>
    )
}

export default ThemeSwitch;