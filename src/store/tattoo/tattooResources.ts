import { create } from "zustand";
import { persist, createJSONStorage } from 'zustand/middleware'
import TattooResources from "@/types/Tattoo/TattooResources";

export const useStore = create<TattooResources>(persist((set, get) => {
    return {
      getBodyLocations: null,
      getTypesTattoo: null,
      getStylesTattoo: null,
      getTypePaiment: null,
      getArtist: null,
      setBodyLocations: (data) => {
        set({ getBodyLocations: data });
      },
      setTypesTattoo: (data) => {
        set({ getTypesTattoo: data });
      },
      setStylesTattoo: (data) => {
        set({ getStylesTattoo: data });
      },
      setTypePaiment: (data) => {
        set({ getTypePaiment: data });
      },
      setArtist: (data) => {
        set({ getArtist: data });
      },
    };
  },
  {
    name: 'TattooResources', // name of the item in the storage (must be unique)
    storage: createJSONStorage(() => sessionStorage), // (optional) by default, 'localStorage' is used
  }
));
