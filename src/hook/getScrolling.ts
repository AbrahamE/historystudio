import { useState } from "react";
import { useScroll, useMotionValueEvent } from "framer-motion";

export default function getScrollingPosition() {
  const [scroll, serScroll] = useState("up");
  const INITIAL_POSITION_SCROLL = 0;
  const { scrollY } = useScroll();

  useMotionValueEvent(scrollY, "change", (latest) => {
    latest > INITIAL_POSITION_SCROLL ? serScroll("down") : serScroll("up");
  });

  return [scroll, serScroll];
}
