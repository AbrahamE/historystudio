"use client";
import { forwardRef } from "react";
import * as Select from "@radix-ui/react-select";
import { CheckIcon, PersonIcon } from "@radix-ui/react-icons";

import Button from "@/components/buttons/DefaultButton";
import SelectDefault from "@/components/form/SelectDefault";

const SelectItem = forwardRef(
  ({ children, className, ...props }: any, forwardedRef: any) => {
    return (
      <Select.Item
        className={`
        ${className}
          text-[13px] leading-none rounded-md flex items-center h-[25px] pr-[35px] pl-[25px]
          relative select-none data-[disabled]:pointer-events-none data-[highlighted]:outline-none
          data-[highlighted]:bg-black data-[highlighted]:text-white
          hover:bg-black hover:text-white"`}
        {...props}
        ref={forwardedRef}
      >
        <Select.ItemText>{children}</Select.ItemText>
        <Select.ItemIndicator className="absolute left-0 w-[25px] inline-flex items-center justify-center">
          <CheckIcon />
        </Select.ItemIndicator>
      </Select.Item>
    );
  }
);

export default function Page() {
  const Tickes = [
    {
      id: 1,
      status_tattoo: "open",
      type: "tattoo",
      name: "primer ticke",
      canvas: "canvas",
      session: 1,
      size: 15,
      body_location: "brazo",
      hours: 2,
      style: "realismo",
      artist: "pedro",
      status_ticket: "open",
      create_ticket: "2021-10-10",
      description:
        "loremp ipsum dolor sit amet consectetur adipisicing elit.  Quisquam, voluptatum. Quisquam, voluptatum. Quisquam, voluptatum.",
    },
  ];

  const tattoo_type = [
    {
      labelGroup: "Tipos",
      Separator: false,
      options: [
        {
          value: "1",
          label: "Blanco y negro",
        },
        {
          value: "1",
          label: "full color",
        },
      ],
    },
  ];
  const payment_type = [
    {
      labelGroup: "Tipos",
      Separator: false,
      options: [
        {
          value: "1",
          label: "Tranferencia",
        },
        {
          value: "1",
          label: "Pago movil",
        },
        {
          value: "1",
          label: "Efectivo",
        },
      ],
    },
  ];

  const body_location = [
    {
      labelGroup: "Localisacion de cuerpo",
      Separator: false,
      options: [
        {
          value: "1",
          label: "brazo",
        },
        {
          value: "2",
          label: "pierna",
        },

        {
          value: "3",
          label: "hombro",
        },
        {
          value: "4",
          label: "cuello",
        },
      ],
    },
  ];
  const style_tattoo = [
    {
      labelGroup: "Estilos de tatuaje",
      Separator: false,
      options: [
        {
          value: "1",
          label: "Minimalista",
        },
        {
          value: "2",
          label: "Geométrico",
        },

        {
          value: "3",
          label: "Microrealismo",
        },
        {
          value: "4",
          label: "Blackwork",
        },
        {
          value: "5",
          label: "Tradicional",
        },
        {
          value: "6",
          label: "Neotradicional",
        },
        {
          value: "7",
          label: "Ilustración",
        },
        {
          value: "8",
          label: "Realismo en sombras",
        },
        {
          value: "9",
          label: "Realismo a color",
        },
        {
          value: "10",
          label: "Acuarelado",
        },
      ],
    },
  ];

  return (
    <section className="px-10 py-5 flex flex-col gap-6 justify-between">
      <nav
        className="sticky top-0 justify-between px-5 py-4 text-gray-700 border 
        border-gray-200 rounded-lg sm:flex sm:px-5  dark:bg-gray-800 
        dark:border-gray-700 backdrop-blur-sm bg-white/30"
        aria-label="Breadcrumb"
      >
        <ol className="inline-flex items-center mb-3 space-x-1 md:space-x-3 sm:mb-0">
          <li>
            <div className="flex items-center">
              <a
                href="#"
                className="ml-1 text-sm font-medium text-gray-700 hover:text-blue-600 md:ml-2 dark:text-gray-400 dark:hover:text-white"
              >
                Home
              </a>
            </div>
          </li>
          <li aria-current="page">
            <div className="flex items-center">
              <svg
                aria-hidden="true"
                className="w-6 h-6 text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clip-rule="evenodd"
                ></path>
              </svg>
              <a
                href="#"
                className="ml-1 text-sm font-medium text-gray-700 hover:text-blue-600 md:ml-2 dark:text-gray-400 dark:hover:text-white"
              >
                invoices
              </a>
            </div>
          </li>
          <li aria-current="page">
            <div className="flex items-center">
              <svg
                aria-hidden="true"
                className="w-6 h-6 text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clip-rule="evenodd"
                ></path>
              </svg>
              <span className="mx-1 text-sm font-medium text-gray-500 md:mx-2 dark:text-gray-400">
                Create
              </span>
            </div>
          </li>
        </ol>
      </nav>

      <article className="flex gap-4 w-full mx-auto text-gray-700 border border-gray-200 rounded-lg bg-white p-16">
        <div
          className="border-dotted border-2 rounded-lg border-gray-400 hover:border-gray-900 w-1/2
          flex justify-center items-center text-center"
        >
          <span className="flex justify-center text-2xl items-center text-center gap-2">
            <PersonIcon className="text-2xl stroke-1 hover:stroke-2" />
            Agregar Cliente
          </span>
        </div>
        <div className="flex flex-col gap-2 grow">
          <div className="flex gap-4">
            <div className="grow">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Fecha de Facturacion
              </label>
              <input
                type="text"
                id="first_name"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="John"
                required
              />
            </div>
            <div className="grow">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Numero de Factura
              </label>
              <input
                type="text"
                id="last_name"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Doe"
                required
              />
            </div>
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
              Fecha de vencimeinto
            </label>
            <input
              type="text"
              id="last_name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="30"
              required
            />
          </div>
        </div>
      </article>

      <div className="w-full mx-auto text-gray-700 border border-gray-200 rounded-lg bg-white p-16">
        <h1 className="text-[21px] pb-5"> Crear Factura </h1>
        <form>
          <article className="grid grid-cols-2 gap-6 mb-6">
            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Nombre
              </label>
              <input
                type="text"
                id="first_name"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="John"
                required
              />
            </div>
            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Apellido
              </label>
              <input
                type="text"
                id="last_name"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Doe"
                required
              />
            </div>
            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Monto
              </label>
              <input
                type="text"
                id="last_name"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="30"
                required
              />
            </div>

            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Numero de telefono
              </label>
              <input
                type="tel"
                id="phone"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="123-45-678"
                pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                required
              />
            </div>

            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                correo electronico
              </label>
              <input
                type="tel"
                id="phone"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="@gmai.com"
                required
              />
            </div>
            <div>
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Metodo de pago
              </label>
              <SelectDefault
                SelectOptions={payment_type}
                placeholder="Metodo de pago"
              />
            </div>
            <span />
            <Button
              className="w-1/2 place-self-end"
              text="Crear nueva factura"
            />
          </article>
        </form>
      </div>
    </section>
  );
}
