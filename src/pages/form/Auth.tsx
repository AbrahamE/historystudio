"use client";
import { useState } from "react";
import { useRouter } from 'next/navigation'
import Image from "next/image";
import * as Form from "@radix-ui/react-form";
import InputDefault from "@/components/form/Input";

// import Button from "@/components/buttons/DefaultButton";
import { Button } from "@nextui-org/react";
import { AuthControllers } from "@/controllers/Auth/AuthController";
import useToastControl from "@/components/toast/toastMeno";

import { MailIcon } from "@/components/icons/MailIcon";
import { EyeFilledIcon } from "@/components/icons/EyeFilledIcon";
import { EyeSlashFilledIcon } from "@/components/icons/EyeSlashFilledIcon";
import { useStore as useStoreUserSession } from "@/store/auth/userSession";
import { createSelectors } from "@/store/index";

export function Auth() {
  const router = useRouter();

  const [user, setUser] = useState("abraham.darkness.escalona@gmail.com");
  const [password, setPassword] = useState("Historystudio#3032");
  const [submit, setSubmit] = useState(false);
  const [submitStatus, setSubmitStatus] = useState(false);

  const [isVisible, setIsVisible] = useState(false);

  const toggleVisibility = () => setIsVisible(!isVisible);

  const userSession = createSelectors(useStoreUserSession);
  const setSession = userSession.use.setSession();

  const { ToastProvider, updateDescription, updateTitle } = useToastControl({
    initialTitle: "success",
    initialDescription: "descricion",
    initialTimeout: 10000,
    initialAction: null
  });

  const handleSubmit = async (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    setSubmit(true);
    const { login } = AuthControllers();
    const { data, error } = await login({ email: user, password });
    if (!error) {
      // @ts-ignore
      setSession(data.session);
      setSubmitStatus(true);
      setTimeout(() => setSubmit(false), 3000);
      updateDescription("success");
      updateTitle("bienvenido");
      router.push("/dashboard");
    };
  };

  return (
    <>
      <Image
        alt="History Tattoo Logo"
        className="pb-10"
        src="/img/HISTORY-TATTOO-LOGO.png"
        width={200}
        height={200}
      />
      <div className="w-full">
        <Form.Root className="flex flex-col gap-4" onSubmit={handleSubmit} >

          <InputDefault
            label="Usuario"
            name="user"
            type="text"
            value={setUser}
            placeholder="email@gmail.com"
            validationState="valid"
            labelPlacement="outside"
            isClearable={true}
            isRequired={true}
            isReadOnly={false}
            isDisabled={false}
            errorMessage=""
            startContent={
              <MailIcon className="text-2xl text-default-400 pointer-events-none flex-shrink-0" />
            }
          />

          <InputDefault
            label="Password"
            name="Password"
            type={isVisible ? "text" : "password"}
            value={setPassword}
            placeholder="Enter your password"
            validationState="valid"
            labelPlacement="outside"
            isClearable={false}
            isRequired={true}
            isReadOnly={false}
            isDisabled={false}
            errorMessage=""
            endContent={
              <button className="flex focus:outline-none" type="button" onClick={toggleVisibility}>
                {isVisible ? (
                  <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                ) : (
                  <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                )}
              </button>
            }
          />


          {/* <Button className={`w-full ${submit && 'disable'}`} text="Ingresar" type="submit" /> */}
          <Button
            size="md" 
            radius="sm" 
            type="submit"
            color={submitStatus ? "success" : "primary"}
            isDisabled={submit}
            isLoading={submit}>
              {submitStatus ? "Bienvenido" : "Ingresar"}
          </Button>
        </Form.Root>

        {ToastProvider}

        <p className="w-full text-center pt-2">
          Registrate{" "}
          <a className="text-blue-600" href="#">
            aqui
          </a>
        </p>
      </div>
    </>
  );
}
