export interface QuotationCollection {
    client: number;
    artist: number;
    document: number;
    quotation_link: number;
}