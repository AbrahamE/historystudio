"use client";
import {
  FaRegChartBar,
  FaRegCalendarAlt,
  FaRegEnvelope,
  FaRegStickyNote,
  FaUsers,
  FaSquare
} from "react-icons/fa";
import Image from "next/image";
import { useRouter } from 'next/navigation'

// import { default as Link } from "@/components/buttons/ButtonLink";
import Link from "next/link";
import ThemeSwitch from "@/components/switch/ThemeSwitch";


import { Listbox, ListboxItem } from "@nextui-org/react";
import { IconWrapper } from "@/components/icons/IconWrapper";
import { BugIcon } from "@/components/icons/BugIcon";
import { PullRequestIcon } from "@/components/icons/PullRequestIcon";
import { ChatIcon } from "@/components/icons/ChatIcon";
import { PlayCircleIcon } from "@/components/icons/PlayCircleIcon";
import { LayoutIcon } from "@/components/icons/LayoutIcon";
import { TagIcon } from "@/components/icons/TagIcon";
import { UsersIcon } from "@/components/icons/UsersIcon";
import { WatchersIcon } from "@/components/icons/WatchersIcon";
import { BookIcon } from "@/components/icons/BookIcon";
import { ChevronRightIcon } from "@/components/icons/ChevronRightIcon";

export default function SideBar({ sidebarEstend, width }: { sidebarEstend: () => void, width: string }) {
  const router = useRouter();
  return (

    <nav className={`block left-0 top-0 bottom-0 bg-white border-r border-gray-300 h-full ${width} items-center ease-out duration-300`}>
      <div
        className="absolute shadow-xl bg-white border z-20
          top-[4.2rem] -right-3 inline-flex items-center justify-center w-6 h-6 overflow-hidden rounded-full hover:bg-gray-200 dark:bg-gray-600"
      >
        <svg
          width="15"
          height="15"
          viewBox="0 0 15 15"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M3.625 7.5C3.625 8.12132 3.12132 8.625 2.5 8.625C1.87868 8.625 1.375 8.12132 1.375 7.5C1.375 6.87868 1.87868 6.375 2.5 6.375C3.12132 6.375 3.625 6.87868 3.625 7.5ZM8.625 7.5C8.625 8.12132 8.12132 8.625 7.5 8.625C6.87868 8.625 6.375 8.12132 6.375 7.5C6.375 6.87868 6.87868 6.375 7.5 6.375C8.12132 6.375 8.625 6.87868 8.625 7.5ZM12.5 8.625C13.1213 8.625 13.625 8.12132 13.625 7.5C13.625 6.87868 13.1213 6.375 12.5 6.375C11.8787 6.375 11.375 6.87868 11.375 7.5C11.375 8.12132 11.8787 8.625 12.5 8.625Z"
            fill="currentColor"
            fill-rule="evenodd"
            clip-rule="evenodd"
          ></path>
        </svg>
      </div>
      <div
        className="absolute shadow-xl  bg-white border z-10
          top-28 -right-2.5 inline-flex items-center justify-center w-5 h-5 overflow-hidden rounded-full hover:bg-gray-200 dark:bg-gray-600"
        onClick={sidebarEstend}
      >
        <svg
          width="15"
          height="15"
          viewBox="0 0 15 15"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M6.18194 4.18185C6.35767 4.00611 6.6426 4.00611 6.81833 4.18185L9.81833 7.18185C9.90272 7.26624 9.95013 7.3807 9.95013 7.50005C9.95013 7.6194 9.90272 7.73386 9.81833 7.81825L6.81833 10.8182C6.6426 10.994 6.35767 10.994 6.18194 10.8182C6.0062 10.6425 6.0062 10.3576 6.18194 10.1819L8.86374 7.50005L6.18194 4.81825C6.0062 4.64251 6.0062 4.35759 6.18194 4.18185Z"
            fill="currentColor"
            fill-rule="evenodd"
            clip-rule="evenodd"
          ></path>
        </svg>
      </div>

      <nav className="h-screen overflow-hidden flex flex-col justify-center items-stretch content-center  ">
        <div className="grow-none self-center pb-5 pt-5">
          <div className="bg-gray-200 rounded-lg h-full">
            <Image
              alt="History Tattoo Logo"
              className="p-2"
              src="/img/htrone.svg"
              width={50}
              height={50}
            />
          </div>
        </div>
        <div className="grow self-center flex justify-center items-center">
          <Listbox
            aria-label="User Menu"
            onAction={(key) => { router.push(key) }}
            className="justify-center items-center p-0 gap-0 divide-y divide-default-300/50 dark:divide-default-100/80 bg-content max-w-[300px] overflow-visible"
            itemClasses={{
              base: "px-3 gap-3 h-12 data-[hover=true]:bg-default-100/80 rounded-none",
            }}
          >
            <ListboxItem
              key="/dashboard"
            // endContent={
            //   <div
            //     className="block text-sm px-5 py-4 hover:text-black"
            //   >
            //     dashboard
            //   </div>
            // }
            >
          <Link href="/dashboard">
              <IconWrapper className="bg-success/10 text-success">
                <BugIcon className="text-lg " />
              </IconWrapper>
          </Link>
            </ListboxItem>
            <ListboxItem
              key="/dashboard/tickets"
            >
              <IconWrapper className="bg-primary/10 text-primary">
                <PullRequestIcon className="text-lg " />
              </IconWrapper>
            </ListboxItem>
            <ListboxItem
              key="/dashboard/accounting"

            >
              <IconWrapper className="bg-secondary/10 text-secondary">
                <ChatIcon className="text-lg " />
              </IconWrapper>
            </ListboxItem>
            <ListboxItem
              key="/dashboard/invoices"

            >
              <IconWrapper className="bg-warning/10 text-warning">
                <PlayCircleIcon className="text-lg " />
              </IconWrapper>
            </ListboxItem>
            <ListboxItem
              key="/dashboard/rrhh"

            >
              <IconWrapper className="bg-default/50 text-foreground">
                <LayoutIcon className="text-lg " />
              </IconWrapper>
            </ListboxItem>

          </Listbox>
          {/* <ul className="py-2 text-sm text-gray-600 dark:text-gray-200">
              <li className="before:block before:absolute before:top-0 before:w-1 before:h-full active:before:bg-black relative inline-block active">
              <Link
                  className="block text-lg px-5 py-4 hover:text-black"
                  href="/dashboard"
                  icon={<FaSquare />}
                  text=""
                  tooltip="Inicio"
                  side="right"
                />
              </li>
              <li>
                <Link 
                  className="block text-lg px-5 py-4 hover:text-black"
                  href="/dashboard/tickets" 
                  icon={ <FaRegStickyNote />}
                  text=""
                  tooltip="tickets"
                  side="right"
                />
              </li>
              <li>
                <Link 
                className="block text-lg px-5 py-4 hover:text-black"
                href="/dashboard/accounting" 
                icon={<FaRegChartBar />}
                text=""
                tooltip="Contabilidad"
                side="right"
                />
              </li>
              <li>
                <Link 
                className="block text-lg px-5 py-4 hover:text-black"
                href="/dashboard/invoices" 
                icon={<FaRegCalendarAlt />}
                text=""
                tooltip="Facturas"
                side="right"
                />
              </li>
              <li>
                <Link 
                  className="block text-lg px-5 py-4 hover:text-black"
                  href="/dashboard/rrhh"
                  icon={<FaUsers />}
                  text=""
                  tooltip="Recursos Humanos"
                  side="right"
                />
              </li>
            </ul> */}
        </div>
        <div className="grow-none self-auto flex justify-center items-center py-5">
          <a href="#" className="block text-lg py-4 hover:text-black">
            <ThemeSwitch />
          </a>
        </div>
      </nav>
    </nav>
  );
}
