import { Selection } from "@nextui-org/react";

export interface DocumentCollection {
  document_number: string;
  status: string;
  amount: number;
  contact_name: string;
  contact_email: string;
  contact_phone?: string;
  contact_address?: string;
  type_document?: number;
  type_paiment?: number;
  notes?: string;
  footer?: string;
}

export interface CreateDocumentCollection {
  name: string;
  email: string;
  type_paiment: string;
  artist: {
    add(value: any): Set;
    clear(): void;
    delete(value: any): boolean;
    entries(): IterableIterator<[any, any]>;
    forEach(callbackfn: (value: any, value2: any, set: Set) => void, thisArg?: any): void;
    has(value: any): boolean;
    keys(): IterableIterator<any>;
    size: number;
    values(): IterableIterator<any>;
  } | Selection;
}
  