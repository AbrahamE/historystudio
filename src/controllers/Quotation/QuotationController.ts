import BaceControllers from "@/controllers/BaceControllers";
import { QuotationCollection } from "@/types/Quotation/QuotationType";
import sha256 from "crypto-js/sha256";

export function QuotationController() {
  const { client, getValueSet } = BaceControllers();

  const BASE_URL = window.location.protocol + "//" + window.location.host;
  const BASE_URI = "/client/quotation/"

  function createLinkQuotationClient(data){
    return sha256(data);
  }

  function createQuotationCollection({ client, artist, document }: QuotationCollection) {
    let [artistSelected] = getValueSet(artist);

    return {
      client,
      artist: artistSelected,
      document,
      quotation_link: BASE_URL + BASE_URI + createLinkQuotationClient({
        client,
        artist,
        document,
      })
    }
  }

  return {
    createQuotationCollection
  }
}
