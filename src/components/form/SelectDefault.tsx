"use client";
import React, { ReactNode } from "react";
import { Select, SelectItem, SelectSection } from "@nextui-org/react";
import {
  CheckIcon,
  ChevronDownIcon,
  ChevronUpIcon,
} from "@radix-ui/react-icons";

const CreateItem = ({ index, title, className, ...props }: any) => {
  return <SelectItem className={`${className}`} {...props} key={index}>{title}</SelectItem>
}
interface SelectDefaultProps {
  className?: string;
  SelectOptions: {
    title: string;
    className: string;
    optionsItems: { [key: string]: any }[];
  }[];
  placeholder?: string;
  SelectIcon?: React.ReactNode;
}

export default function SelectDefault(props: SelectDefaultProps) {
  const headingClasses = "flex w-full sticky top-1 z-20 py-1.5 px-2 bg-primary text-white shadow rounded-md px-4";
  
  return (
    <Select
      label="Favorite Animal"
      labelPlacement="outside"
      className="w-full"
      size="md"
      radius="sm"
      scrollShadowProps={{ isEnabled: false }}
      listboxProps={{ itemClasses: { base: ["rounded-md  px-4"] } }}
      popoverProps={{
        classNames: { base: "rounded-md px-4 border-small border-divider bg-background", arrow: "bg-default-200" }
      }}
    >
      {props.SelectOptions.map(({ title, className, optionsItems, ...props }) => (
        <SelectSection
          title={title}
          classNames={{ heading: headingClasses }}
        >
          {optionsItems.map(({ title, className, ...props }, index: number) =>
            CreateItem({ index, title, className, ...props })
          )}
        </SelectSection>
      ))}
    </Select>
  );
}
