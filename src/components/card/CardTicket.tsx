"use client";
import { Card, CardHeader, CardBody, CardFooter, Divider, Image } from "@nextui-org/react";
import { CardTickeInterfacetProps } from "@/types/TicketType";

import AvatarUser from "@/components/avatar/AvatarUser";

export default function CardTicket({
  index,
  Title,
  Description,
  Footer,
  FooterTrigger,
  Headers,
  HeadersTrigger,
  HeadersClassName,
  BodyClassName,
  FooterClassName,
  onPress
}: CardTickeInterfacetProps) {
  return (
    <>
      <Card className="max-w-[400px] rounded-lg" isPressable onPress={onPress} key={index}>
        <CardHeader className={`flex justify-between gap-3 ${HeadersClassName}`}>
          <div className="grow self-auto">
            <Image
              alt="nextui logo"
              height={40}
              radius="sm"
              src="https://avatars.githubusercontent.com/u/86160567?s=200&v=4"
              width={40}
            />
          </div>
          <div className="grow self-auto flex gap-2 justify-end items-center">
            {Headers}
            {HeadersTrigger}
          </div>
        </CardHeader>
        <Divider />
        <CardBody className={`${BodyClassName} `}>
          <div className="flex flex-col">
            <h1 className="text-xl font-bold">{Title}</h1>
            <p className="text-sm text-gray-500">{Description}</p>
          </div>
        </CardBody>
        <Divider />
        <CardFooter className={`${FooterClassName} flex items-center justify-between px-5 py-3 gap-2`}>
          {/* <section className="flex -space-x-4">{Footer}</section>
          <div></div> */}
          <AvatarUser/>
          <div className="text-small tracking-tight text-default-400">{FooterTrigger}</div>
        </CardFooter>
      </Card>
    </>
  );
}
