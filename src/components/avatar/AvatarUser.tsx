"use client";
import Image from "next/image";
import { User } from "@nextui-org/react";

interface AvatarUserProps {
  src: string;
  alt: string;
  UserName: string;
}

export default function AvatarUser({ src, alt, UserName }: AvatarUserProps) {
  
  return <User
    name={UserName}
    description="@jrgarciadev"
    classNames={{
      name: "text-default-600",
      description: "text-default-500",
    }}
    avatarProps={{
      size: "sm",
      src: "https://avatars.githubusercontent.com/u/30373425?v=4",
    }}
  />;
}
