"use client";
import { useState } from "react";
import DefaultGraphic from "@/components/graphics/DefaultGraphic";
import HeaderSearsh from "@/components/nav/HeaderSearsh";
import CardPrices from "@/components/card/CardPrices";

import BaceControllers from "@/controllers/BaceControllers";
// import {
//   ScheduleComponent,
//   Day,
//   Week,
//   WorkWeek,
//   Month,
//   Agenda,
//   Inject,
// } from "@syncfusion/ej2-react-schedule";

import { FaBookmark } from "react-icons/fa";

export default function Page() {
  const [progress, setProgress] = useState(13);
  const [search, setSearch] = useState("");
  const [person, setPerson] = useState("Pedro");
  const [date, setDate] = useState(null);

  let xAxisData = [];
  let data1 = [];
  let data2 = [];
  for (var i = 0; i < 100; i++) {
    xAxisData.push("A" + i);
    data1.push((Math.sin(i / 5) * (i / 5 - 10) + i / 6) * 5);
    data2.push((Math.cos(i / 5) * (i / 5 - 10) + i / 6) * 5);
  }

  const user = async () => {
    const { client } = BaceControllers();

    const { data: { user } } = await client.auth.getUser()
    console.log(user);
  }

  const sigOut = async () => {
    const { client } = BaceControllers();

    let { error } = await client.auth.signOut()
    console.log(error);
  }

  const echar1 = {
    title: {
      text: "Bar Animation Delay",
    },
    legend: {
      data: ["bar", "bar2"],
    },
    toolbox: {
      // y: 'bottom',
      feature: {
        magicType: {
          type: ["stack"],
        },
        dataView: {},
        saveAsImage: {
          pixelRatio: 2,
        },
      },
    },
    tooltip: {},
    xAxis: {
      data: xAxisData,
      splitLine: {
        show: false,
      },
    },
    yAxis: {},
    series: [
      {
        name: "bar",
        type: "bar",
        data: data1,
        emphasis: {
          focus: "series",
        },
        animationDelay: function (idx: any) {
          return idx * 10;
        },
      },
      {
        name: "bar2",
        type: "bar",
        data: data2,
        emphasis: {
          focus: "series",
        },
        animationDelay: function (idx: any) {
          return idx * 10 + 100;
        },
      },
    ],
    animationEasing: "elasticOut",
    animationDelayUpdate: function (idx: any) {
      return idx * 5;
    },
  };

  const echar2 = {
    tooltip: {
      trigger: "item",
    },
    legend: {
      top: "5%",
      left: "center",
    },
    series: [
      {
        name: "Access From",
        type: "pie",
        radius: ["40%", "70%"],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: "#fff",
          borderWidth: 2,
        },
        label: {
          show: false,
          position: "center",
        },
        emphasis: {
          label: {
            show: true,
            fontSize: 20,
            fontWeight: "bold",
          },
        },
        labelLine: {
          show: false,
        },
        data: [
          { value: 1000, name: "Search Engine" },
          { value: 500, name: "Direct" },
          { value: 580, name: "Email" },
        ],
      },
    ],
  };

  const echar3 = {
    tooltip: {
      trigger: "item",
    },
    legend: {
      top: "5%",
      left: "center",
    },
    series: [
      {
        name: "Access From",
        type: "pie",
        radius: ["40%", "70%"],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: "#fff",
          borderWidth: 2,
        },
        label: {
          show: false,
          position: "center",
        },
        emphasis: {
          label: {
            show: true,
            fontSize: 20,
            fontWeight: "bold",
          },
        },
        labelLine: {
          show: false,
        },
        data: [
          { value: 500, name: "Search Engine" },
          { value: 500, name: "Direct" },
        ],
      },
    ],
  };

  return (
    <>
      <HeaderSearsh disengaged={false} setSearch={setSearch} setProgress={setProgress} />
      <main className="flex flex-col gap-6 justify-between px-7 py-10 w-auto ease-out duration-300">
        <section className="grow flex gap-6 justify-between">
          <div className="grow flex gap-6 ">
            <CardPrices
              title="TOTAL INGRESOS"
              currency="$0.00"
              icon={<FaBookmark />}
              typePayment="Cuentas por cobrar"
              typeCurrency="$0.00 / $0.00"
            />
            <CardPrices
              title="TOTAL DE GASTOS"
              currency="$0.00"
              icon={<FaBookmark />}
              typePayment="Cuentas por cobrar"
              typeCurrency="$0.00 / $0.00"
            />
            <CardPrices
              title="BENEFICIO TOTAL"
              currency="$0.00"
              icon={<FaBookmark />}
              typePayment="Cuentas por cobrar"
              typeCurrency="$0.00 / $0.00"
            />

          </div>
        </section>
        <section className="grow flex gap-6 justify-between">
          <div className="grow">
            <DefaultGraphic width={1090} height={400} option={echar1} />
          </div>

          {/* <div className="grow w-2/4">
            <div className="bg-white w-full p-5 rounded-lg">
              <Calendar
                className="w-full"
                value={date}
                onChange={(e: any) => setDate(e.value)}
                inline
                showWeek
              />
            </div>
          </div> */}
        </section>
        <section className="grow flex gap-6 justify-between">
          <div className="grow w-1/2">
            Ingresos por Categoría
            <DefaultGraphic width={500} height={400} option={echar2} />
          </div>
          <div className="grow w-1/2">
            Gastos por Categoría
            <DefaultGraphic width={500} height={400} option={echar3} />
          </div>
        </section>

        {/* <section className="grow flex gap-6 justify-between">
          <ScheduleComponent>
            <Inject services={[Day, Week, WorkWeek, Month, Agenda]} />
          </ScheduleComponent>
        </section> */}
      </main>
    </>
  );
}
