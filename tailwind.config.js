const colors = require("tailwindcss/colors");
const {nextui} = require("@nextui-org/react");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/**/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/**/**/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/**/**/**/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/**/**/**/**/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    backgroundColor: (theme) => ({
      ...theme("colors"),
      primary: {
        light: "#ccdce9",
        dark: "#175795",
        DEFAULT: "#000",
      },
      secondary: {
        light: "#fefeff",
        dark: "#e14651",
        DEFAULT: "#000", //#ef4b4c
      },
      terseary: {
        light: "#e85c00",
        dark: "#e85c00",
        DEFAULT: "#fff27a",
      },
      complementary: {
        light: "#fde6aa",
        dark: "#ba2716",
        DEFAULT: "#e9e9eb !important",
      },
    }),
    fill: ({ theme }) => ({
      ...theme("colors"),
    }),
    colors: {
      ...colors,
      primary: {
        light: "#ccdce9",
        dark: "#175795",
        DEFAULT: "#136FA1",
      },
      secondary: {
        light: "#fefeff",
        dark: "#e14651",
        DEFAULT: "#ef4b4c",
      },
      terseary: {
        light: "#e85c00",
        dark: "#e85c00",
        DEFAULT: "#43506c",
      },
      complementary: {
        light: "#fde6aa",
        dark: "#ba2716",
        DEFAULT: "#e9e9eb",
      },
    },
    extend: {
      minHeight: {
        "screen-75": "75vh",
        "screen-70": "70vh",
        "screen-65": "65vh",
        "screen-60": "60vh",
        "screen-55": "55vh",
        "screen-50": "50vh",
      },
      opacity: {
        80: ".8",
        70: ".7",
        60: ".6",
        50: ".5",
      },
      zIndex: {
        60: "60",
        70: "70",
        80: "80",
        90: "90",
        100: "100",
        110: "110",
        120: "120",
      },
      inset: {
        "-100": "-100%",
        "-225-px": "-225px",
        "-160-px": "-160px",
        "-150-px": "-150px",
        "-94-px": "-94px",
        "-50-px": "-50px",
        "-29-px": "-29px",
        "-20-px": "-20px",
        "25-px": "25px",
        "40-px": "40px",
        "95-px": "95px",
        "145-px": "145px",
        "195-px": "195px",
        "210-px": "210px",
        "260-px": "260px",
      },
      height: {
        "95-px": "95px",
        "70-px": "70px",
        "300-px": "300px",
        "350-px": "350px",
        "600-px": "600px",
        "650-px": "650px",
        "900-px": "900px",
        "925-px": "925px",
        "950-px": "950px",
        "1000-px": "600px",
        95: "98%",
      },
      maxWidth: {
        "100-px": "100px",
        "120-px": "120px",
        "150-px": "150px",
        "180-px": "180px",
        "200-px": "200px",
        "210-px": "210px",
        "580-px": "580px",
        100: "-100%",
        "4/5": "60rem",
        "1/5": "22rem",
      },
      minWidth: {
        "140-px": "140px",
        48: "12rem",
      },
      maxHeight: {
        "860-px": "860px",
      },
      backgroundSize: {
        full: "100%",
      },
      backgroundColor: ["active"],
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      flex: {
        2: "1 2 auto",
      },
      fontFamily: {
        mont: ["var(--font-montserrat)"],
      },
    },
    objectPosition: {
      left: "left",
      "left-top": "left top",
      "left-center": "left center",
      "left-bottom": "left bottom",
      right: "right",
      "right-top": "right top",
      "right-center": "right center",
      "right-bottom": "right bottom",
      top: "top",
      "top-left": "top left",
      "top-center": "top center",
      "top-right": "top right",
      bottom: "bottom",
      "bottom-left": "bottom left",
      "bottom-center": "bottom center",
      "bottom-right": "bottom right",
      center: "center",
      "center-top": "center top",
      "center-left": "center left",
      "center-right": "center right",
      "center-bottom": "center bottom",
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      "2xl": "1536px",
    },
    keyframes: {
      slideUpAndFade: {
        '0%': { opacity: 0, transform: 'translateY(2px)' },
        '100%': { opacity: 1, transform: 'translateY(0)' },
      },
      slideRightAndFade: {
        '0%': { opacity: 0, transform: 'translateX(-2px)' },
        '100%': { opacity: 1, transform: 'translateX(0)' },
      },
      slideDownAndFade: {
        '0%': { opacity: 0, transform: 'translateY(-2px)' },
        '100%': { opacity: 1, transform: 'translateY(0)' },
      },
      slideLeftAndFade: {
        '0%': { opacity: 0, transform: 'translateX(2px)' },
        '100%': { opacity: 1, transform: 'translateX(0)' },
      },
      hide: {
        from: { opacity: 1 },
        to: { opacity: 0 },
      },
      slideIn: {
        from: { transform: 'translateX(calc(100% + var(--viewport-padding)))' },
        to: { transform: 'translateX(0)' },
      },
      swipeOut: {
        from: { transform: 'translateX(var(--radix-toast-swipe-end-x))' },
        to: { transform: 'translateX(calc(100% + var(--viewport-padding)))' },
      },
    },
    animation: {
      slideUpAndFade: 'slideUpAndFade 300ms cubic-bezier(0.16, 0, 0.13, 1)',
      slideDownAndFade: 'slideDownAndFade 300ms cubic-bezier(0.16, 0, 0.13, 1)',
      slideRightAndFade: 'slideRightAndFade 300ms cubic-bezier(0.16, 0, 0.13, 1)',
      slideLeftAndFade: 'slideLeftAndFade 300ms cubic-bezier(0.16, 0, 0.13, 1)',
      hide: 'hide 100ms ease-in',
      slideIn: 'slideIn 150ms cubic-bezier(0.16, 1, 0.3, 1)',
      swipeOut: 'swipeOut 100ms ease-out',
    },
    boxShadow: {
      '3xl': [
        'hsla(206,22%,7%,35%) 0px 10px 38px -10px',
        'hsla(206,22%,7%,20%) 0px 10px 20px -15px'
      ],
    }
  },
  plugins: [nextui()],
};
