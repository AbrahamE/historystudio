"use client";
import { useState, Fragment } from "react";
import {
  DotsVerticalIcon,
  CheckIcon,
  GridIcon,
  TableIcon,
  ListBulletIcon,
} from "@radix-ui/react-icons";
import { Card, CardHeader, CardBody, CardFooter, Image, Button, ButtonGroup, Link } from "@nextui-org/react";

import DropdownMenu from "@/components/dropdown/DropdownMenu";
import HeaderSearsh from "@/components/nav/HeaderSearsh";
import AvatarUser from "@/components/avatar/AvatarUser";
import CardTicket from "@/components/card/CardTicket";
import ButtonGroups from "@/components/buttons/ButtonGroups";
import TableActions from "@/components/tables/TableActions";
import PageLayoutSubNav from "@/layouts/pages/PageLayoutSubNav";
import NavTickes from "@/widgets/nav/NavTickes";
import TabsSection from "@/components/tabs/TabsSection";
import CardGrid from "@/widgets/grid/CardGrid";
import { motion, AnimatePresence, LayoutGroup } from "framer-motion";

import { DeleteDocumentIcon } from "@/components/icons/DeleteDocumentIcon"
import { EditDocumentIcon } from "@/components/icons/EditDocumentIcon"
import { CopyDocumentIcon } from "@/components/icons/CopyDocumentIcon"

import { TickesInterface } from "@/types/TicketType";

export default function Page() {
  const [progress, setProgress] = useState(13);

  const [typeView, setTypeView] = useState<"Grid" | "Table">("Grid");
  const [search, setSearch] = useState("");
  const [person, setPerson] = useState("Pedro");
  const [summary, setSummary] = useState<TickesInterface | null | {}>(null);
  const [selectedId, setSelectedId] = useState<string | null>(null);

  const Tickets: TickesInterface[] = [
    {
      id: 1,
      status_tattoo: "open",
      type: "tattoo",
      name: "Primer ticket",
      canvas: "canvas",
      session: 1,
      size: 15,
      body_location: "brazo",
      hours: 2,
      style: "realismo",
      artist: "Sugeidy",
      status_ticket: "open",
      create_ticket: "2021-10-10",
      description:
        "La descripción del tatuaje (Es la historia de porque se quere hacer el tajuaje)",
    },
    {
      id: 2,
      status_tattoo: "open",
      type: "tattoo",
      name: "Segundo ticket",
      canvas: "canvas",
      session: 1,
      size: 15,
      body_location: "brazo",
      hours: 2,
      style: "realismo",
      artist: "Maria",
      status_ticket: "open",
      create_ticket: "2021-10-10",
      description:
        "La descripción del tatuaje (Es la historia de porque se quere hacer el tajuaje)",
    },
  ];

  const menuOptions = {
    options: [
      {
        name: "Editar",
        handleClick: () => console.log("Editar"),
        icon: <EditDocumentIcon />,
      },
      {
        name: "Reasignar ",
        handleClick: () => console.log("Reasignar"),
        icon: <CopyDocumentIcon />,
      },
      {
        name: "Eliminar",
        handleClick: () => console.log("Eliminar"),
        icon: <DeleteDocumentIcon />,
      },
    ]
  }


  const groups = [
    { value: "board", handleClick: () => { }, icon: <GridIcon /> },
    { value: "table", handleClick: () => { }, icon: <TableIcon /> },
    { value: "list", handleClick: () => { }, icon: <ListBulletIcon /> },
  ];

  const MyTabs = {
    TabsList: [
      {
        tab: 'darf',
        className: "darf-tab"
      },
      {
        tab: 'prosessin',
        className: "prosessin-tab"
      },
      {
        tab: 'success',
        className: "success-tab"
      },
      {
        tab: 'closed',
        className: "closed-tab"
      },

    ],
    TabsSection: {
      section: <div className="px-5">
        <ButtonGroup>
          <Button onClick={() => setTypeView("Grid")} radius="sm" size="sm">
            Grid <GridIcon />
          </Button>
          <Button onClick={() => setTypeView("Table")} radius="sm" size="sm">
            Table <TableIcon />
          </Button>
        </ButtonGroup>
      </div>,
      className: 'section-1',
    },
    TabsContent: [
      {
        content: <>
          
          {typeView === "Grid"
            ? <main className="grid grid-cols-4 content-stretch place-content-stretch gap-4 p-5 h-screen">           
              {Tickets.map((ticket, index) => (
                  <CardGrid
                    tickes={{ ...ticket, index }}
                    person={person}
                    menuOptions={menuOptions}
                    setSummary={setSummary}
                    setSelectedId={setSelectedId}
                  />
                ))}
              </main>
            : <TableActions />
          }
        </>,
        contentName: 'darf',
        className: 'content-1',
      },
      {
        content: "Content 2",
        contentName: 'prosessin',
        className: 'content-2',
      },
      {
        content: "Content 3",
        contentName: 'success',
        className: 'content-3',
      },
      {
        content: "Content 4",
        contentName: 'closed',
        className: 'content-4',
      }
    ],
    TabsRoot: {
      className: 'tabs-root',
    },
    defaultValue: 'darf',
  };

  return (
    <PageLayoutSubNav
      header={
        <HeaderSearsh
          disengaged={false}
          setSearch={setSearch}
          setProgress={setProgress}
        />
      }
      subHeader={<NavTickes progress={progress} />}
    >
      <section className="relative grow flex justify-between ease-out duration-300">

        <LayoutGroup>
          <TabsSection {...MyTabs} />
          {/* <div class="relative h-full w-full"> */}

          <AnimatePresence>
            {selectedId && (
              <motion.div
                className="fixed flex h-screen inset-0 p-28 z-[100] backdrop-blur-sm bg-black/20"

              >
                <motion.div className="grow self-auto w-full h-full " layoutId={selectedId}>
                  <Card className="h-3/4 rounded-lg shadow-3xl">
                    <CardHeader className="pb-0 pt-2 px-4 flex-col items-start">
                      <p className="text-tiny uppercase font-bold">Daily Mix</p>
                      <small className="text-default-500">12 Tracks</small>
                      <h4 className="font-bold text-large">Frontend Radio</h4>
                    </CardHeader>

                    <CardBody className="flex flex-row overflow-visible py-2 ">
                      <div classname="grow self-auto">
                        {summary.id}
                        {summary.index}
                        {summary.name}
                        {summary.description}
                        {summary.status_tattoo}

                        <motion.button
                          className="h-5 w-5 bg-black"
                          onClick={() => setSelectedId(null)}
                        />
                      </div>
                      <div classname="grow self-auto w-2/4">
                        <Card
                          isFooterBlurred
                          radius="lg"
                          className="border-none"
                        >
                          <Image
                            alt="Woman listing to music"
                            className="object-cover"
                            height={200}
                            src="https://images.unsplash.com/photo-1570168983832-8989dae1522e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1yZWxhdGVkfDR8fHxlbnwwfHx8fHw%3D&auto=format&fit=crop&w=400&q=60"
                            width={200}
                          />
                          <CardFooter className="justify-between before:bg-white/10 border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10">
                            <p className="text-tiny text-white/80">Available soon.</p>
                            <Button className="text-tiny text-white bg-black/20" variant="flat" color="default" radius="lg" size="sm">
                              Notify me
                            </Button>
                          </CardFooter>
                        </Card>
                      </div>
                    </CardBody>
                    <CardFooter>
                      <Link
                        isExternal
                        showAnchorIcon
                        href="https://github.com/nextui-org/nextui"
                      >
                        Visit source code on GitHub.
                      </Link>
                    </CardFooter>
                  </Card>
                </motion.div>
              </motion.div>
            )}
          </AnimatePresence>
          {/* </div> */}
        </LayoutGroup>

      </section>
    </PageLayoutSubNav>
  );
}
