export interface LocationOfTheBodyForTattoo {
  id: number;
  name: string;
  created_at: string;
}

export interface ArtistTattoo {
  id: number;
  name: string;
  user: string;
  commission_percentage: number;
  created_at: string;
}

export interface TattooTechnique {
  id: number;
  name: string;
  created_at: string;
}

export interface TattooStyle {
  id: number;
  name: string;
  in_color: boolean;
  in_shadows: boolean;
  created_at: string;
}

export interface getTypePaiment {
  id: number;
  name: string;
  created_at: string;
}

export default interface TattooResources {
  getBodyLocations: LocationOfTheBodyForTattoo[] | null;
  getTypesTattoo: ArtistTattoo[] | null;
  getStylesTattoo: TattooTechnique[] | null;
  getTypePaiment:  getTypePaiment[] | null;
  getArtist: TattooStyle[] | null;
  setBodyLocations: (data: TattooResources["getBodyLocations"]) => void;
  setTypesTattoo: (data: TattooResources["getTypesTattoo"]) => void;
  setStylesTattoo: (data: TattooResources["getStylesTattoo"]) => void;
  setTypePaiment: (data: TattooResources["getTypePaiment"]) => void;
  setArtist: (data: TattooResources["getArtist"]) => void;
}
