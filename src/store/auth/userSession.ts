import { create } from "zustand";
import { persist, createJSONStorage } from 'zustand/middleware'
import { UserAuthInterface , UserActions } from "@/types/User/AuthUser";

export const useStore = create<UserAuthInterface & UserActions>(persist((set) => {
    return {
      getUser: null,
      getSession: null,
      setUser:(data) => {
        set({ getUser: data });
      },
      setSession:(data) => {
        set({ getSession: data });
      },
    };
  },
  {
    name: 'userSession', // name of the item in the storage (must be unique)
    storage: createJSONStorage(() => sessionStorage), // (optional) by default, 'localStorage' is used
  }
));
