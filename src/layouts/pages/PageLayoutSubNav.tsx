interface PageLayoutSubNavProps {
  children: React.ReactNode;
  header: React.ReactNode;
  subHeader: React.ReactNode;
  footer?: React.ReactNode;
  className?: string;
}

export default function PageLayoutSubNav({
  children,
  header,
  subHeader,
  footer,
  className,
}: PageLayoutSubNavProps) {
  return (
    <main>
      {header} {subHeader}
      <section className={className}>{children}</section>
      {footer}
    </main>
  );
}
