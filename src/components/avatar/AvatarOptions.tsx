"use client";
import DropdownMenu from "@/components/dropdown/DropdownMenu";
import { useStore as useStoreResources } from "@/store/tattoo/tattooResources";
import { useStore as useStoreUserSession } from "@/store/auth/userSession";
import { createSelectors } from "@/store/index";
import { User } from "@nextui-org/react";
import { FaBookmark, FaRegSun, FaBell } from "react-icons/fa";

export default function AvatarOptions() {

  const resources = createSelectors(useStoreResources);
  const userSession = createSelectors(useStoreUserSession);

  const artist = resources.use.getArtist() ?? [];
  const userAuth = userSession.use.getUser() ?? [];
  
  const menuOptions = {
    options: [
      {
        name: "Artistas",
        handleClick: () => console.log("Editar"),
        icons: "",
        shortcut: "⌘A"
      },
      {
        name: "Roles",
        handleClick: () => console.log("Reasignar"),
        icons: "",
        shortcut: "⌘R"
      },
      {
        name: "Permisos",
        handleClick: () => console.log("Eliminar"), 
        icons: "",
        shortcut: "⌘P"       
      },
      {
        name: "Configuraciones",
        handleClick: () => console.log("Eliminar"),   
        icon: <FaRegSun/>,
        shortcut: "⌘S"     
      },
    ]
  }

  let dropdownOptions = {
    className: "",
    itemClasses: [],
    user: false,
    logoutOption: true
  }

  return (
    <div className="flex items-center gap-4">
      <DropdownMenu 
        Trigger={
          <User
            as="button"
            avatarProps={{
              isBordered: true,
              src: `https://api.multiavatar.com/${userAuth.user?.id.split("-")[0]}.png`,
            }}
            description={userAuth.user?.email?.split("@")[0] ?? ""}
            name={artist.find((art)=> art.user == userAuth.user.id)?.name}
          />
        }
        DropdownItems={menuOptions}
        DropdownOptions={dropdownOptions}
      />
        {/* https://i.pravatar.cc/150?u=a042581f4e29026024d */}
    </div>
  );
}
