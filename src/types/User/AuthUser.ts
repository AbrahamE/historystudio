export interface AuthUser {
  email: string;
  password: string;
}

export interface identities {
  created_at: string;
  id: string;
  identity_data: {
    email: string;
    sub: string;
  };
  last_sign_in_at: string;
  provider: string;
  updated_at: string;
  user_id: string;
}

export interface User {
  app_metadata: {
    provider: string;
    providers: string[];
  };
  auth_flaud: string;
  confirmed_at: string;
  created_at: string;
  email: string;
  email_confirmed_at: string;
  id: string;
  identities: identities[];
  last_sign_in_at: string;
  phone?: string | null;
  role: string;
  updated_at: string;
  user_metadata?: Object[] | null;
}

export interface UserAuthInterface {
  user?: User | null;
  session?: {
    access_token: string;
    expires_at: number;
    expires_in: number;
    refresh_token: string;
    token_type: string;
    user: User;
  } | null;
}

export interface UserActions {
  setUser: (data: UserAuthInterface["user"]) => void;
  setSession: (data: UserAuthInterface["session"]) => void;
}

