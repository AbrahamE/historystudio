"use client";
import { useEffect, useState } from "react";
import { Badge } from "@nextui-org/react";
import Input from "@/components/form/Input";
import * as Form from "@radix-ui/react-form";
import { FaBookmark, FaBell } from "react-icons/fa";
import { SearchIcon } from "@/components/icons/SearchIcon";

import Header from "@/layouts/nav/LayoutHeader";
import AvatarOptions from "@/components/Avatar/AvatarOptions";

interface HeaderSearshProps {
  disengaged?: boolean;
  setSearch: any;
  setProgress: any;
}

export default function HeaderSearsh({
  disengaged,
  setSearch,
  setProgress,
}: HeaderSearshProps) {
  const [bookmarksChecked, setBookmarksChecked] = useState(true);
  const [urlsChecked, setUrlsChecked] = useState(false);
  const [person, setPerson] = useState("Pedro");
  const [isInvisible, setIsInvisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => setProgress(66), 500);
    return () => clearTimeout(timer);
  }, []);

  const menuOptions = [
    {
      name: "Profiel",
      icon: <FaBookmark />,
    },
    {
      name: "Artist",
      icon: <FaBookmark />,
    },
    {
      name: "Roles",
      icon: <FaBookmark />,
    },
    {
      name: "Permisos",
      icon: <FaBookmark />,
    },
  ];

  return (
    <Header disengaged={disengaged} className="py-3 px-5 grow top-0 bg-white backdrop-blur-sm z-50 w-full ease-out duration-300">
      <div className="flex justify-between gap-2 ">
        <Form.Root className="w-1/3">
          <Input
            placeholder="Searsh"
            value={setSearch}
            name="searsh"
            type="text"
            validationState="valid"
            startContent={
              <SearchIcon  className="text-black/50 dark:text-white/90 text-slate-400 pointer-events-none flex-shrink-0" />
            }
          />
        </Form.Root>
        <div className="grow flex items-center justify-end gap-4 w-1/3">
          <Badge content="new" color="danger" isInvisible={isInvisible} size="sm">
            <div className={`inline-flex items-center justify-center w-10 h-10 overflow-hidden rounded-full hover:bg-gray-200 dark:bg-gray-600
              ${(!isInvisible) && 'ring-offset-2 ring-2 ring-rose-600 bg-gray-200'}`}>
              <button
                className="font-medium text-gray-600 dark:text-gray-300"
                onClick={() => setIsInvisible(!isInvisible)}
              >
                <FaBell />
              </button>
            </div>
          </Badge>
          <section>
            <AvatarOptions
              UserName={person}
              Icon={{ fa: <FaBookmark /> }}
              menuOptions={menuOptions}
            />
          </section>
        </div>
      </div>
    </Header>
  );
}
