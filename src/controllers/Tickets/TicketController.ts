import { BaceControllers } from "../BaceControllers";

export class TicketController extends BaceControllers {
  constructor() {
    super();
  }

  async getTickets() {
    const { data, error } = await this.SUPABASE.from("tickets").select("*");
    if (error) return error;
    return { data };
  }

  async getTicketById(id: string) {
    const { data, error } = await this.SUPABASE.from("tickets")
      .select("*")
      .eq("id", id);
    if (error) return error;
    return { data };
  }

  async getTicketsByUserId(user_id: string) {
    const { data, error } = await this.SUPABASE.from("tickets")
      .select("*")
      .eq("user_id", user_id);
    if (error) return error;
    return { data };
  }

  async createTicket(title: string, description: string, user_id: string) {
    const { data, error } = await this.SUPABASE.from("tickets").insert([
      {
        title,
        description,
        user_id,
      },
    ]);
    if (error) return error;
    return { data };
  }

  async updateTicket(id: string, title: string, description: string) {
    const { data, error } = await this.SUPABASE.from("tickets")
      .update({ title, description })
      .eq("id", id);
    if (error) return error;
    return { data };
  }

  async updateTicketStatus(id: string, status: string) {
    const { data, error } = await this.SUPABASE.from("tickets")
      .update({ status })
      .eq("id", id);
    if (error) return error;
    return { data };
  }

  async deleteTicket(id: string) {
    const { data, error } = await this.SUPABASE.from("tickets")
      .delete()
      .eq("id", id);
    if (error) return error;
    return { data };
  }
}
