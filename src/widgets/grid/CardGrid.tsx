"use client";
import {
  DotsVerticalIcon,
  CheckIcon
} from "@radix-ui/react-icons";
import DropdownMenu from "@/components/dropdown/DropdownMenu";

import AvatarUser from "@/components/avatar/AvatarUser";
import CardTicket from "@/components/card/CardTicket";
import TableActions from "@/components/tables/TableActions";
import { motion } from "framer-motion";
import { TickesInterface } from "@/types/TicketType";
import { Button, Chip } from "@nextui-org/react";

interface cardGridProps {
  tickes: TickesInterface;
  person: any;
  menuOptions: any;
  setSummary: any;
  setSelectedId: any;
}

export default function cardGrid({
  tickes: {
    index, id, name, description, status_tattoo
  },
  person, menuOptions, setSummary, setSelectedId
}: cardGridProps) {

  let dropdownOptions = {
    className: "",
    itemClasses: [],
    user: false,
  }

  return <>
    <div key={index} className="card">
      <motion.div
        initial={{ opacity: 0, scale: 0.8, y: 0.5 }}
        animate={{ opacity: 1, scale: 1, y: 1 }}
        transition={{
          duration: 0.8,
          delay: 0.5,
          ease: [0, 0.71, 0.2, 1.01]
        }}
        layoutId={id.toString()}
      >
      
      <CardTicket
          key={index}
          index={index}
          Title={name}
          Description={description}
          Headers={<Chip size="lg" radius="sm">{status_tattoo}</Chip>}
          onPress={() => {
            console.log("algo");
            setSelectedId(id.toString());
            setSummary({ index, id, name, description, status_tattoo, person });
          }}
          HeadersTrigger={
            <DropdownMenu 
              Trigger={
                <Button isIconOnly color="default" variant="flat" aria-label="opctions">
                  <DotsVerticalIcon />
                </Button>
              }
              DropdownItems={menuOptions}
              DropdownOptions={dropdownOptions}
            />
          }
          Footer={
            <AvatarUser
              alt={person}
              UserName={person}
              src={"/img/midas.jpg"}
            />
          }
          FooterTrigger={
            <motion.div className="flex justify-center gap-2 items-center cursor-pointer">
              <motion.div
                aria-label="Customise options"
                className="inline-flex justify-center items-center w-10 h-10 overflow-hidden bg-gray-100 rounded-full dark:bg-gray-600"
              >
                <motion.span className="font-medium text-gray-600 dark:text-gray-300">
                  <CheckIcon />
                </motion.span>
              </motion.div>
            </motion.div>
          }
        />
      </motion.div> 
    </div>
  </>
}