export interface TickesCollection {
  id: number;
  status_tattoo: number;
  status_ticket: number;
  type_tattoo: number;
  style_tattoo: number;
  document: number;
  client: number;
  artist: number;
  session_tattoo: number;
  size_tattoo: string;
  estimated_hours: number;
  body_location: number;
  description: string;
  create_ticket: string;
  description: string;
}

export interface CardTickeInterfacetProps {
  index?: number | undefined;
  Title?: string | null;
  Description?: string | null;
  BodyClassName?: string | null;
  Headers?: any;
  HeadersTrigger?: any | null;
  HeadersClassName?: string | null;
  Footer?: any | null;
  FooterTrigger?: any | null;
  FooterClassName?: string | null;
  onPress?: () => void | null | undefined;
}