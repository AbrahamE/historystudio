const URL_SEPARATOR_DEFAULT: string = "/";

export function getUrlArrayPathNameSlice(url: string | null) {
  const pathArray = url
    ?.split(URL_SEPARATOR_DEFAULT)
    .filter((path: string) => path)
    .reduce((oldArray: string[], newElement: string) => {
      return [
        ...oldArray,
        oldArray.slice(-1) + newElement + URL_SEPARATOR_DEFAULT,
      ];
    }, []);

  return pathArray;
}

export function getUrlArrayPathName(url: string | null) {
  const pathArray = url
    ?.split(URL_SEPARATOR_DEFAULT)
    .filter((path: string) => path)
    .reduce((oldArray: string[], newElement: string) => {
      return [...oldArray, newElement];
    }, []);

  return pathArray;
}
