"use client";
import Link from "next/link";
import * as Tooltip from "@radix-ui/react-tooltip";
import { motion } from "framer-motion";

interface ButtonLinkProps {
  href: string;
  icon?: any;
  text: string;
  tooltip?: string;
  side?: string;
  className?: string;
}

function ButtonsTooltip({
  href,
  icon,
  text,
  side,
  tooltip,
  className,
}: ButtonLinkProps) {
  return (
    <Tooltip.Provider>
      <Tooltip.Root>
        <Tooltip.Trigger asChild>
          <motion.div
            whileHover={{ scale: 1.02 }}
            whileTap={{ scale: 0.99 }}
            transition={{ type: "spring", stiffness: 400, damping: 10 }}
          >
            {link({ href, icon, text, className })}
          </motion.div>
        </Tooltip.Trigger>
        <Tooltip.Portal>
          <Tooltip.Content
            className="
            data-[state=delayed-open]:data-[side=top]:animate-slideDownAndFade 
            data-[state=delayed-open]:data-[side=right]:animate-slideLeftAndFade 
            data-[state=delayed-open]:data-[side=left]:animate-slideRightAndFade 
            data-[state=delayed-open]:data-[side=bottom]:animate-slideUpAndFade 
            select-none rounded-lg bg-black px-[15px] py-[10px] text-[15px] 
            text-white leading-none shadow will-change-[transform,opacity]"
            sideOffset={2}
            side={side}
          >
            {tooltip}
            <Tooltip.Arrow />
          </Tooltip.Content>
        </Tooltip.Portal>
      </Tooltip.Root>
    </Tooltip.Provider>
  );
}

function Buttons({ href, icon, text, className }: ButtonLinkProps) {
  return (
    <motion.div
      whileHover={{ scale: 1.02 }}
      whileTap={{ scale: 0.99 }}
      transition={{ type: "spring", stiffness: 400, damping: 10 }}
    >
      {link({ href, icon, text, className })}
    </motion.div>
  );
}

function link({ href, icon, text, className }: ButtonLinkProps) {
  return (
    <Link
      href={href}
      className={
        className ? className : "block text-lg px-5 py-4 hover:text-black"
      }
    >
      {icon}
      {text}
    </Link>
  );
}

export default function ButtonsLink({
  href,
  icon,
  text,
  side,
  tooltip,
  className,
}: ButtonLinkProps) {
  return (
    <>
      {tooltip
        ? ButtonsTooltip({
            href,
            icon,
            text,
            side,
            tooltip,
            className,
          })
        : Buttons({
            href,
            icon,
            text,
            tooltip,
            className,
          })}
    </>
  );
}
