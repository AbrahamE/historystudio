"use client";
import { useState } from "react";
import * as Tabs from "@radix-ui/react-tabs";

import {
  DotsVerticalIcon,
  CheckIcon,
  Pencil1Icon,
  SwitchIcon,
  TrashIcon,
  EyeOpenIcon
} from "@radix-ui/react-icons";
import DropdownMenu from "@/components/dropdown/DropdownMenu";
import HeaderSearsh from "@/components/nav/HeaderSearsh";
import AvatarUser from "@/components/Avatar/AvatarUser";
import CardTicket from "@/components/card/CardTicket";
import ButtonLink from "@/components/buttons/ButtonLink";


import { FaBookmark, FaPlus } from "react-icons/fa";
import * as Progress from "@radix-ui/react-progress";

interface Tickes {
  id: number;
  status_tattoo: string;
  type: string;
  name: string;
  canvas: string;
  session: number;
  size: number;
  body_location: string;
  hours: number;
  style: string;
  artist: string;
  status_ticket: string;
  create_ticket: string;
  description: string;
}

export default function Page() {
  const [progress, setProgress] = useState(13);
  const [search, setSearch] = useState("");
  const [person, setPerson] = useState("Pedro");

  const Tickes: Tickes[] = [
    {
      id: 1,
      status_tattoo: "open",
      type: "tattoo",
      name: "primer ticke",
      canvas: "canvas",
      session: 1,
      size: 15,
      body_location: "brazo",
      hours: 2,
      style: "realismo",
      artist: "pedro",
      status_ticket: "open",
      create_ticket: "2021-10-10",
      description:
        "loremp ipsum dolor sit amet consectetur adipisicing elit.  Quisquam, voluptatum. Quisquam, voluptatum. Quisquam, voluptatum.",
    },
    {
      id: 1,
      status_tattoo: "open",
      type: "tattoo",
      name: "primer ticke",
      canvas: "canvas",
      session: 1,
      size: 15,
      body_location: "brazo",
      hours: 2,
      style: "realismo",
      artist: "pedro",
      status_ticket: "open",
      create_ticket: "2021-10-10",
      description:
        "loremp ipsum dolor sit amet consectetur adipisicing elit.  Quisquam, voluptatum. Quisquam, voluptatum. Quisquam, voluptatum.",
    },
  ];

  const menuOptions = [
    {
      name: "Ver",
      icon: <EyeOpenIcon />,
      href: "/dashboard/invoices/see"
    },
    {
      name: "Editar",
      icon: <Pencil1Icon />,
      href: "/dashboard/invoices/see"
    },
    {
      name: "Reasignar ",
      icon: <SwitchIcon />,
      href: "/dashboard/invoices/see",
      separator: true
    },
    {
      name: "Eliminar",
      icon: <TrashIcon />,
      href: "/dashboard/invoices/see"
    },
    
  ];

  return (
    <>
      <HeaderSearsh
        
        setSearch={setSearch}
        setProgress={setProgress}
      />

      <section className="grow flex justify-between ">
        <Tabs.Root className="relative flex flex-col w-full" defaultValue="tab1">
          <Tabs.List
            className="shrink-0 flex w-full border-b border-gray-200 pt-4 bg-white"
            aria-label="Manage your account"
          >
            <Tabs.Trigger
              className="bg-white px-5 py-2 h-[45px] flex grow-none items-center justify-center text-[15px] data-[state=active]:text-violet11 data-[state=active]:shadow-[inset_0_-1px_0_0,0_1px_0_0] data-[state=active]:shadow-current data-[state=active]:focus:relative data-[state=active]:focus:shadow-black cursor-default"
              value="tab1"
            >
              Listado de Facturas 
            </Tabs.Trigger>
            {/* <Tabs.Trigger
              className="bg-white px-5 h-[45px] flex grow-none items-center justify-center text-[15px] hover:text-violet11 data-[state=active]:text-violet11 data-[state=active]:shadow-[inset_0_-1px_0_0,0_1px_0_0] data-[state=active]:shadow-current data-[state=active]:focus:relative data-[state=active]:focus:shadow-black cursor-default"
              value="tab2"
            >
              in progress 
            </Tabs.Trigger> */}
            <section className="bg-white grow flex justify-end px-5 py-2">
              <ButtonLink
                className="absolute -top-1 flex items-center justify-center gap-2 text-white bg-black hover:bg-gray-800 focus:ring-4 focus:ring-gray-400 rounded-lg text-sm p-3 dark:bg-white dark:hover:bg-gray-200 focus:outline-none dark:focus:ring-gray-800"
                href="/dashboard/invoices/create"
                icon={<FaPlus />}
                text="Crear nueva factura"
                tooltip="Crear nueva factura"
              />
            </section>
          </Tabs.List>

          <Tabs.Content className="grow p-5" value="tab2">
            <main className="grid grid-cols-4 content-stretch place-content-stretch gap-4 p-5">
              {Tickes.map((ticke, index) => {
                return (
                  <CardTicket
                    index={index}
                    Title={ticke.name}
                    Description={ticke.description}
                    Headers={ticke.status_tattoo}
                    HeadersTrigger={
                      <DropdownMenu
                        Trigger={
                          <div className="flex justify-center gap-2 items-center cursor-pointer">
                            <div
                              aria-label="Customise options"
                              className="inline-flex justify-center items-center w-5 h-5 overflow-hidden"
                            >
                              <span className="font-medium text-gray-600 dark:text-gray-300">
                                <DotsVerticalIcon />
                              </span>
                            </div>
                          </div>
                        }
                        DropdownItems={menuOptions}
                      />
                    }
                    Footer={
                      <AvatarUser
                        alt={person}
                        UserName={person}
                        src={"/img/midas.jpg"}
                      />
                    }
                    FooterTrigger={
                      <div className="flex justify-center gap-2 items-center cursor-pointer">
                        <div
                          aria-label="Customise options"
                          className="inline-flex justify-center items-center w-10 h-10 overflow-hidden bg-gray-100 rounded-full dark:bg-gray-600"
                        >
                          <span className="font-medium text-gray-600 dark:text-gray-300">
                            <CheckIcon />
                          </span>
                        </div>
                      </div>
                    }
                  />
                );
              })}
            </main>
          </Tabs.Content>

          <Tabs.Content className="grow p-10" value="tab1">
            <div className="relative p-5 bg-white overflow-x-auto shadow rounded-lg">
              <div className="flex items-center justify-between pb-4 dark:bg-gray-900">
                <div>
                  <button
                    id="dropdownActionButton"
                    data-dropdown-toggle="dropdownAction"
                    className="inline-flex items-center text-gray-500 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-3 py-1.5 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                    type="button"
                  >
                    <span className="sr-only">filtro</span>
                    Action
                    <svg
                      className="w-3 h-3 ml-2"
                      aria-hidden="true"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M19 9l-7 7-7-7"
                      ></path>
                    </svg>
                  </button>

                  <div
                    id="dropdownAction"
                    className="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700 dark:divide-gray-600"
                  >
                    <ul
                      className="py-1 text-sm text-gray-700 dark:text-gray-200"
                      aria-labelledby="dropdownActionButton"
                    >
                      <li>
                        <a
                          href="#"
                          className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                        >
                          Reward
                        </a>
                      </li>
                      <li>
                        <a
                          href="#"
                          className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                        >
                          Promote
                        </a>
                      </li>
                      <li>
                        <a
                          href="#"
                          className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white"
                        >
                          Activate account
                        </a>
                      </li>
                    </ul>
                    <div className="py-1">
                      <a
                        href="#"
                        className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
                      >
                        Delete User
                      </a>
                    </div>
                  </div>
                </div>
                <label className="sr-only">Search</label>
                <div className="relative">
                  <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg
                      className="w-5 h-5 text-gray-500 dark:text-gray-400"
                      aria-hidden="true"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                  </div>
                  <input
                    type="text"
                    id="table-search-users"
                    className="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Search for users"
                  />
                </div>
              </div>
              <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                  <tr>
                    <th scope="col" className="p-4">
                      
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Artista 
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Fecha de creacion
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Estatus
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Opciones 
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td className="w-4 p-4">
                      
                    </td>
                    <th
                      scope="row"
                      className="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      <div className="pl-3">
                        <div className="text-base font-semibold">sugeidy</div>
                        
                      </div>
                    </th>
                    <td className="px-6 py-4">12-3-2023</td>
                    <td className="px-6 py-4">
                      <div className="flex items-center">
                        <div className="h-2.5 w-2.5 rounded-full bg-green-500 mr-2"></div>{" "}
                        Pagada
                      </div>
                    </td>
                    <td className="px-6 py-4">
                    <DropdownMenu
                        Trigger={
                          <div className="flex justify-center gap-2 items-center cursor-pointer">
                            <div
                              aria-label="Customise options"
                              className="inline-flex justify-center items-center w-5 h-5 overflow-hidden"
                            >
                              <span className="font-medium text-gray-600 dark:text-gray-300">
                                <DotsVerticalIcon />
                              </span>
                            </div>
                          </div>
                        }
                        DropdownItems={menuOptions}
                      />
                    </td>
                  </tr>
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td className="w-4 p-4">
                    
                    </td>
                    <th
                      scope="row"
                      className="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      <div className="pl-3">
                        <div className="text-base font-semibold">Daniel</div>
                        
                      </div>
                    </th>
                    <td className="px-6 py-4">12-3-2023</td>
                    <td className="px-6 py-4">
                      <div className="flex items-center">
                        <div className="h-2.5 w-2.5 rounded-full bg-green-500 mr-2"></div>{" "}
                        Pagada
                      </div>
                    </td>
                    <td className="px-6 py-4">
                    <DropdownMenu
                        Trigger={
                          <div className="flex justify-center gap-2 items-center cursor-pointer">
                            <div
                              aria-label="Customise options"
                              className="inline-flex justify-center items-center w-5 h-5 overflow-hidden"
                            >
                              <span className="font-medium text-gray-600 dark:text-gray-300">
                                <DotsVerticalIcon />
                              </span>
                            </div>
                          </div>
                        }
                        DropdownItems={menuOptions}
                      />
                    </td>
                  </tr>
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td className="w-4 p-4">
                      
                    </td>
                    <th
                      scope="row"
                      className="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      <div className="pl-3">
                        <div className="text-base font-semibold">Maria</div>
                        
                      </div>
                    </th>
                    <td className="px-6 py-4">12-3-2023</td>
                    <td className="px-6 py-4">
                      <div className="flex items-center">
                        <div className="h-2.5 w-2.5 rounded-full bg-green-500 mr-2"></div>{" "}
                        Pagada
                      </div>
                    </td>
                    <td className="px-6 py-4">
                    <DropdownMenu
                        Trigger={
                          <div className="flex justify-center gap-2 items-center cursor-pointer">
                            <div
                              aria-label="Customise options"
                              className="inline-flex justify-center items-center w-5 h-5 overflow-hidden"
                            >
                              <span className="font-medium text-gray-600 dark:text-gray-300">
                                <DotsVerticalIcon />
                              </span>
                            </div>
                          </div>
                        }
                        DropdownItems={menuOptions}
                      />
                    </td>
                  </tr>
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td className="w-4 p-4">
                      
                    </td>
                    <th
                      scope="row"
                      className="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      <div className="pl-3">
                        <div className="text-base font-semibold">Marivel</div>
                        
                      </div>
                    </th>
                    <td className="px-6 py-4">12-3-2023</td>
                    <td className="px-6 py-4">
                      <div className="flex items-center">
                        <div className="h-2.5 w-2.5 rounded-full bg-green-500 mr-2"></div>{" "}
                        Pagada
                      </div>
                    </td>
                    <td className="px-6 py-4">
                    <DropdownMenu
                        Trigger={
                          <div className="flex justify-center gap-2 items-center cursor-pointer">
                            <div
                              aria-label="Customise options"
                              className="inline-flex justify-center items-center w-5 h-5 overflow-hidden"
                            >
                              <span className="font-medium text-gray-600 dark:text-gray-300">
                                <DotsVerticalIcon />
                              </span>
                            </div>
                          </div>
                        }
                        DropdownItems={menuOptions}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </Tabs.Content>
        </Tabs.Root>
      </section>
    </>
  );
}
