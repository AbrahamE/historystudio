"use client";
import { ReactNode, useEffect, useState } from "react";
import SideBar from "@/components/nav/SideBar";
import { TattooControllers } from "@/controllers/Tattoo/TattooController";
import { AuthControllers } from "@/controllers/Auth/AuthController";
import { UserControllers } from "@/controllers/Auth/UserController";
import { useStore as useStoreResources } from "@/store/tattoo/tattooResources";
import { useStore as useStoreUserSession } from "@/store/auth/userSession";
import { createSelectors } from "@/store/index";
export default function RootLayout({ children }: { children: ReactNode }) {
  const { getBodyLocations, getTypesTattoo, getStylesTattoo, getArtist, getTypePaiment } = TattooControllers();
  const { getSession } = AuthControllers();
  const { getUser } = UserControllers();
  const resources = createSelectors(useStoreResources);
  const userSession = createSelectors(useStoreUserSession);

  const userAuth = userSession.use.getUser();
  const session = userSession.use.getSession();

  const setUserAuth = userSession.use.setUser();
  const setSession = userSession.use.setSession();

  const bodyLocations = resources.use.getBodyLocations();
  const typesTattoo = resources.use.getTypesTattoo();
  const stylesTattoo = resources.use.getStylesTattoo();
  const typePaiment = resources.use.getTypePaiment();
  const artist = resources.use.getArtist();

  const setBodyLocations = resources.use.setBodyLocations();
  const setTypesTattoo = resources.use.setTypesTattoo();
  const setStylesTattoo = resources.use.setStylesTattoo();
  const setTypePaiment = resources.use.setTypePaiment();
  const setArtist = resources.use.setArtist();

  const resourcesTattoo = async () => {
    try {
      if (!userAuth?.length && !session?.length) {
        const { data: user, error: userError } = await getUser();
        const { data: session, error: sessionError } = await getSession();
        // @ts-ignore
        !userError && setUserAuth(user);
        // @ts-ignore
        !sessionError && setSession(session);
      }

      if (
        !userAuth?.length &&
        !session?.length &&
        !bodyLocations?.length &&
        !typesTattoo?.length &&
        !stylesTattoo?.length &&
        !artist?.length &&
        !typePaiment?.length
      ) {
        const { data: body_locations, error: bodyLocationsError } = await getBodyLocations();
        const { data: types_tattoo, error: typesTattooError } = await getTypesTattoo();
        const { data: styles_tattoo, error: stylesTattooError } = await getStylesTattoo();
        const { data: type_paiment, error: typePaimentError } = await getTypePaiment();
        const { data: artist_tattoo, error: artistError } = await getArtist();
        // @ts-ignore
        !bodyLocationsError && setBodyLocations(body_locations);
        // @ts-ignore
        !typesTattooError && setTypesTattoo(types_tattoo)
        // @ts-ignore
        !stylesTattooError && setStylesTattoo(styles_tattoo)
        // @ts-ignore
        !typePaimentError && setTypePaiment(type_paiment)
        // @ts-ignore
        !artistError && setArtist(artist_tattoo)
      }
    } catch (error) {
      console.log(error);
    }
  }
  const [w, setW] = useState(false);

  const sidebarEstend = () => setW(!w);

  useEffect(() => {
    resourcesTattoo()
  }, [])

  return (
    <>
      <main className="relative flex flex-row grow content-center items-stretch justify-between h-full w-full">
        <nav className={`sticky top-0 flex flex-col flex-none justify-stretch items-stretch ${!w ? "w-14" : "w-1/4"} max-w-1/4 h-screen ease-out duration-300 z-30`}>
          <SideBar sidebarEstend={sidebarEstend} width={!w ? "w-14" : "w-full"} />
        </nav>
        <article className="relative flex flex-col grow content-center items-stretch justify-stretch grow self-auto max-w-[100%] w-full h-auto overflow-x-hidden bg-gray-200 w-4/5">
          {children}
        </article>
      </main>
    </>
  );
}
