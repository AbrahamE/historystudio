"use client";

export default function FormLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <article
      className="grow 
        relative 
        flex 
        justify-center 
        align-center
        place-items-center 
        font-mont
        before:absolute
        before:h-[180px]
        before:w-[240px]
        before:-translate-x-1/2
        before:rounded-full
        before:bg-gradient-radial
        before:from-white
        before:to-transparent 
        before:blur-2xl 
        before:content-[''] 
        before:dark:bg-gradient-to-br 
        before:dark:from-transparent 
        before:dark:to-blue-700 
        before:dark:opacity-10 
        after:absolute 
        after:h-[280px] 
        after:w-[340px] 
        after:translate-x-1/3 
        after:bg-gradient-conic 
        after:from-sky-200 
        after:via-blue-200 
        after:blur-2xl 
        after:content-[''] 
        after:dark:from-sky-900 
        after:dark:via-sky-900 
        after:dark:opacity-40"
    >
      {children}
    </article>
  );
}
