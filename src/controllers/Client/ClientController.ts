import BaceControllers from "@/controllers/BaceControllers";
import { ArtistTattoo } from "@/types/Tattoo/TattooResources";

import { useStore } from "@/store/Tattoo/tattooResources";
import { createSelectors } from "@/store/index";

interface CreateDocumentCollection { 
  name: string, 
  email: string, 
  last_name?: string | null, 
  phone?: number | null 
}

export function ClientController() {
  const { client } = BaceControllers();

  async function createClient(client: CreateDocumentCollection){
    return client.from('client').upsert(client).select()
  }

  function createClientCollection({ name, last_name, email, phone }: CreateDocumentCollection) {
    return {
      name, last_name, email, phone
    }
  }

  return {
    createClientCollection,
    createClient
  }
}
