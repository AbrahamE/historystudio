"use client";
import { useState, useMemo, useEffect } from "react";
import { PersonIcon, CheckIcon } from "@radix-ui/react-icons";
import * as Form from "@radix-ui/react-form";
import { Button, Snippet } from "@nextui-org/react";

import Breadcrumbs from "@/components/nav/Breadcrumbs"
import InputDefault from "@/components/form/Input";
import CardDefault from "@/components/card/CardDefault";
import DropdownSelect from "@/components/dropdown/DropdownSelect";

import { Select, SelectItem , Selection } from "@nextui-org/react";

import { DocumentController } from "@/controllers/Document/DocumentController"
import { ClientController } from "@/controllers/Client/ClientController"
import { QuotationController } from "@/controllers/Quotation/QuotationController"

import { useStore } from "@/store/Tattoo/tattooResources";
import { createSelectors } from "@/store/index";

export default function Page() {
  const resources = createSelectors(useStore);
  const documentController = DocumentController();
  const clientController = ClientController();
  const quotationController = QuotationController();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [size, setSize] = useState("");
  const [tattooType, setTattooType] = useState<Selection>(new Set([]));
  const [tattooStyle, setTattooStyle] = useState<Selection>(new Set([]));
  const [bodyLocations, setBodyLocations] = useState<Selection>(new Set([]));
  const [typePaiment, setTypePaiment] = useState<Selection>(new Set([]));
  const [artist, setArtist] = useState<Selection>(new Set([]));
  const [submit, setSubmit] = useState(false);
  const [submitStatus, setSubmitStatus] = useState(false);
  const [urlGenerated, setUrlGenerated] = useState("https://");

  const storeBodyLocations = resources.use.getBodyLocations() ?? [];
  const storeTypesTattoo = resources.use.getTypesTattoo() ?? [];
  const storeStylesTattoo = resources.use.getStylesTattoo() ?? [];
  const storeTypePaiment = resources.use.getTypePaiment() ?? [];
  const storeArtist = resources.use.getArtist() ?? [];

  const Quotation = {
    contact_name: "",
    artist_name: "",
    quotation_link: "",
    invoice: "",
  }

  const Tickes = {
    status_tattoo: "",
    status_ticket: "",
    type_tattoo: "",
    style_tattoo: "",
    invoice: "",
    client: "",
    artist: "",
    session_tattoo: "",
    size_tattoo: "",
    estimated_hours: "",
    body_location: "",
    description: "",
  };

  const handleSubmit = async (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    setSubmit(true);

    const Client = clientController.createClientCollection(
      { name, email, last_name: null, phone: null }
    );
    //@ts-ignore
    const Invoice = documentController.createDocumentCollection(
      { name, email, type_paiment: typePaiment, artist }
    );

    const Quotation = quotationController.createQuotationCollection(
      { client: name, artist, document: 1 }
    );

    console.log(Invoice, Client, Quotation);
   
    setTimeout(() => {
      setSubmit(false)
      setSubmitStatus(true);
    }, 3000);
  }

  return (
    <>
      <Breadcrumbs />
      <section className="flex px-10 pb-5 flex-col gap-6 justify-between">
        <article className="flex flex-col w-full px-10 gap-4 mx-auto border border-gray-200 rounded-lg bg-white p-16">
          <h1 className="text-xl pb-10">Cliente avitual</h1>
          <div className="flex gap-6">
            <div
              className="block border-2 border-black border-dashed text-black rounded text-xl select-none py-[45px] w-2/4 text-center"
            >
              <span className="flex justify-center text-2xl items-center text-center gap-2">
                <PersonIcon className="text-2xl stroke-1 hover:stroke-2" />
                Usuario Avitual
              </span>
            </div>
            <div className="flex flex-col grow text-end w-2/4 gap-6">
              <div className="flex flex-row gap-7 py-6">
                <InputDefault
                  label="Nombre del cliente"
                  name="nombre"
                  type="text"
                  className={["grow", "self-auto", "w-2/4"]}
                  value={setName}
                  defaultValue={name}
                  validationState="valid"
                  labelPlacement="outside"
                  isClearable={true}
                  isRequired={false}
                  isReadOnly={false}
                  isDisabled={false}
                  errorMessage=""
                />
                <InputDefault
                  label="Apellido del cliente"
                  name="apellido"
                  type="text"
                  className={["grow", "self-auto", "w-2/4"]}
                  value={setEmail}
                  defaultValue={email}
                  validationState="valid"
                  labelPlacement="outside"
                  isClearable={false}
                  isRequired={false}
                  isReadOnly={false}
                  isDisabled={false}
                  errorMessage=""
                />
              </div>
              <div className="flex flex-row gap-7 py-6">
                <InputDefault
                  label="Numero de telefono"
                  name="phone"
                  type="text"
                  className={["grow", "self-auto", "w-2/4"]}
                  value={setName}
                  defaultValue={name}
                  validationState="valid"
                  labelPlacement="outside"
                  isClearable={true}
                  isRequired={false}
                  isReadOnly={false}
                  isDisabled={false}
                  errorMessage=""
                />
                <InputDefault
                  label="email"
                  name="email"
                  type="email"
                  className={["grow", "self-auto", "w-2/4"]}
                  value={setEmail}
                  defaultValue={email}
                  validationState="valid"
                  labelPlacement="outside"
                  isClearable={false}
                  isRequired={false}
                  isReadOnly={false}
                  isDisabled={false}
                  errorMessage=""
                  endContent={
                    <div className="pointer-events-none flex items-center">
                      <span className="text-default-400 text-small">@gmail.com</span>
                    </div>
                  }
                />
              </div>
            </div>
          </div>
        </article>

        <Form.Root className="flex flex-col gap-6" onSubmit={handleSubmit}>
          <CardDefault className="w-full"
            header={<h1 className="text-xl"> Genera cotisacion </h1>} 
            footer={
              <div className="grow self-auto flex flex-wrap gap-4 justify-between items-center ease-out duration-300">
                <Snippet disableCopy={!submitStatus} 
                  tooltipProps={{ size:"md" }}
                  className="grow"
                  size="md" 
                  radius="sm" 
                  variant="shadow" 
                  color={submitStatus ? "success" : "primary"}>
                  {urlGenerated}
                </Snippet>
                <div className={`${submitStatus ? "grow-0" : "grow"} flex justify-end ease-out duration-300`}>
                  <Button
                    type="submit"
                    radius="sm"
                    className="grow-0"
                    color="primary"
                    isDisabled={submit}
                    isLoading={submit}
                    endContent={submitStatus && <CheckIcon/>}
                  >
                    {submitStatus ? "Generada" : "Generar cotizacion"}
                  </Button>
                </div> 
              </div> 
            }
            >
                <div className="flex flex-row gap-7">

                <div className="grow self-auto w-2/4 py-6">
                <InputDefault
                  label="Nombre"
                  name="nombre"
                  type="text"
                  className={["grow", "self-auto", "w-2/4"]}
                  value={setName}
                  defaultValue={name}
                  validationState="valid"
                  labelPlacement="outside"
                  isClearable={true}
                  isRequired={true}
                  isReadOnly={false}
                  isDisabled={false}
                  errorMessage=""
                />
                </div>
                <div className="grow self-auto w-2/4 py-6">
                <InputDefault
                  label="email"
                  name="email"
                  type="email"
                  className={["grow", "self-auto", "w-2/4"]}
                  value={setEmail}
                  defaultValue={email}
                  validationState="valid"
                  labelPlacement="outside"
                  isClearable={false}
                  isRequired={true}
                  isReadOnly={false}
                  isDisabled={false}
                  errorMessage=""
                  endContent={
                    <div className="pointer-events-none flex items-center">
                      <span className="text-default-400 text-small">@gmail.com</span>
                    </div>
                  }
                />
                </div>
              </div>

              <div className="flex flex-row gap-7">

                <div className="grow self-auto w-2/4 py-6">
                  <Select
                    items={storeTypesTattoo}
                    labelPlacement="outside"
                    label="Tipo de tatuaje"
                    className="w-full"
                    size="md"
                    radius="sm"
                    scrollShadowProps={{ isEnabled: false }}
                    listboxProps={{ itemClasses: { base: ["rounded-md  px-4"] } }}
                    value={tattooType}
                    isRequired
                    onSelectionChange={(art) => setTattooType(art)}
                    popoverProps={{
                      classNames: { base: "rounded-md px-4 border-small border-divider bg-background", arrow: "bg-default-200" }
                    }}
                  >
                    {(item) => (
                      <SelectItem key={item.name} value={item.id} className="capitalize">
                        {item.name}
                      </SelectItem>
                    )}
                  </Select>
                </div>

                <div className="grow self-auto w-2/4 py-6">
                  <Select
                    items={storeStylesTattoo}
                    labelPlacement="outside"
                    label="Estilo del tatuaje"
                    className="w-full"
                    size="md"
                    radius="sm"
                    scrollShadowProps={{ isEnabled: false }}
                    listboxProps={{ itemClasses: { base: ["rounded-md  px-4"] } }}
                    value={tattooStyle}
                    isRequired
                    onSelectionChange={(art) => setTattooStyle(art)}
                    popoverProps={{
                      classNames: { base: "rounded-md px-4 border-small border-divider bg-background", arrow: "bg-default-200" }
                    }}
                  >
                    {(item) => (
                      <SelectItem key={item.name} value={item.id} className="capitalize">
                        {item.name}
                      </SelectItem>
                    )}
                  </Select>
                </div>
              </div>

              <div className="flex flex-row gap-7 items-end">
                <div className="grow self-auto w-2/4 py-6">
                  <Select
                    items={storeBodyLocations}
                    labelPlacement="outside"
                    label="Localizacion del cuerpo"
                    className="w-full"
                    size="md"
                    radius="sm"
                    scrollShadowProps={{ isEnabled: false }}
                    listboxProps={{ itemClasses: { base: ["rounded-md  px-4"] } }}
                    value={bodyLocations}
                    isRequired
                    onSelectionChange={(art) => setBodyLocations(art)}
                    popoverProps={{
                      classNames: { base: "rounded-md px-4 border-small border-divider bg-background", arrow: "bg-default-200" }
                    }}
                  >
                    {(item) => (
                      <SelectItem key={item.name} value={item.id} className="capitalize">
                        {item.name}
                      </SelectItem>
                    )}
                  </Select>
                </div>

                <div className="grow self-auto w-2/4 py-6">
                  <InputDefault
                    label="Tamaño tatuaje"
                    name="size"
                    type="text"
                    className={["grow", "self-auto", "w-2/4"]}
                    value={setSize}
                    defaultValue={size}
                    validationState="valid"
                    labelPlacement="outside"
                    isClearable={true}
                    isRequired={true}
                    isReadOnly={false}
                    isDisabled={false}
                    errorMessage=""
                    endContent={
                      <div className="pointer-events-none flex items-center">
                        <span className="text-default-400 text-small">cm</span>
                      </div>
                    }
                  />
                </div>
              </div>

              <div className="flex flex-row gap-7">
                <div className="grow self-auto w-2/4 py-6">
                  <Select
                    items={storeArtist}
                    labelPlacement="outside"
                    label="Artistas"
                    className="w-full"
                    size="md"
                    radius="sm"
                    scrollShadowProps={{ isEnabled: false }}
                    listboxProps={{ itemClasses: { base: ["rounded-md  px-4"] } }}
                    value={artist}
                    isRequired
                    onSelectionChange={(art) => setArtist(art)}
                    popoverProps={{
                      classNames: { base: "rounded-md px-4 border-small border-divider bg-background", arrow: "bg-default-200" }
                    }}
                  >
                    {(item) => (
                      <SelectItem key={item.name} value={item.id} className="capitalize">
                        {item.name}
                      </SelectItem>
                    )}
                  </Select>
                 
                </div>
                <div className="grow self-auto w-2/4 py-6">
                  <Select
                    items={storeTypePaiment}
                    labelPlacement="outside"
                    label="Tipo de pago"
                    className="w-full"
                    size="md"
                    radius="sm"
                    scrollShadowProps={{ isEnabled: false }}
                    listboxProps={{ itemClasses: { base: ["rounded-md  px-4"] } }}
                    value={typePaiment}
                    isRequired
                    onSelectionChange={(art) => setTypePaiment(art)}
                    popoverProps={{
                      classNames: { base: "rounded-md px-4 border-small border-divider bg-background", arrow: "bg-default-200" }
                    }}
                  >
                    {(item) => (
                      <SelectItem key={item.name} value={item.id} className="capitalize">
                        {item.name}
                      </SelectItem>
                    )}
                  </Select>
                </div>
              </div>

            </CardDefault>
          </Form.Root>
      </section>
    </>
  );
}

