import BaceControllers from "@/controllers/BaceControllers";
import { CreateDocumentCollection, DocumentCollection } from "@/types/Document/DocumentType";

export function DocumentController() {
  const { client, getValueSet, storeApp } = BaceControllers();
  const { storeArtist, storeTypePaiment } = storeApp;

  let PREFIX_DOCUMENT: string = "IVC";
  let QUANTITY_ZEROS_DOCUMENT_NUMBER: number = 5;
  let PERCENTEGE: number = 30;
  let DEFAULT_STATUS_DOCUMENT: number = 1;
  let DEFAULT_TYPE_DOCUMENT: number = 1;

  function generateInvoiceNumber(lastInvoiceNumber: number) {
    return PREFIX_DOCUMENT + lastInvoiceNumber.toString().padStart(
      QUANTITY_ZEROS_DOCUMENT_NUMBER,
      '0'
    );
  }

  function createTotalTattooPrice(total: number) {
    let amount = (PERCENTEGE / 100) * total;

    return [total, amount]
  }

  function createDocumentCollection(
    { name, email, type_paiment, artist }: CreateDocumentCollection
  ): DocumentCollection {
    // @ts-ignore
    let [artistSelected] = getValueSet(artist);
    let [typePaimentSelected] = getValueSet(type_paiment);

    const [total, amount] = createTotalTattooPrice(
      storeArtist
        .find(art => art.name == artistSelected)?.commission_percentage ?? 0
    )

    const getTypePaiment = storeTypePaiment
      .find(art => art.name == typePaimentSelected)?.id ?? 0

    return {
      document_number: generateInvoiceNumber(1),
      status: DEFAULT_STATUS_DOCUMENT,
      amount: total + amount,
      contact_name: name,
      contact_email: email,
      type_document: DEFAULT_TYPE_DOCUMENT,
      type_paiment: getTypePaiment,
    }
  }

  return {
    generateInvoiceNumber,
    createTotalTattooPrice,
    createDocumentCollection
  }
}
