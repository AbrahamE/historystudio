import { createColumnHelper } from "@tanstack/react-table";

export const useColumnHelper = <T>() => {
  const columnHelper = createColumnHelper<T>();

  return [columnHelper];
};

interface useConfigColumnProps {
  columnHelper: any;
  configColumn: Record<string, string>[];
}

export const useConfigColumn = <T>({
  columnHelper,
  configColumn,
}: useConfigColumnProps) => {
  const columns: T = configColumn.map((colName) =>
    columnHelper.accessor((row: { [x: string]: any }) => row[colName], {
      id: colName,
      cell: (info: any) => info.getValue(),
      header: (info: any) => info.getValue(),
      footer: (info: any) => info.getValue(),
    })
  );

  return columns;
};
