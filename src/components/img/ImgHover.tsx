"use client";
import React from "react";
import Image from 'next/image'
// @ts-ignore
import hoverEffect from "hover-effect";
interface ImgHoverProps {
  imgOne: string | any;
  imgTwo: string | any;
  type: number;
}

export default function ImgHover({ imgOne, imgTwo, type }: ImgHoverProps): React.ReactElement {
  // const mydisplacementImage = [
  //   "/img/texture/text1.jpg",
  //   "/img/texture/text2.jpg",
  //   "/img/texture/text3.jpg",
  //   "/img/texture/text4.jpg",
  //   "/img/texture/text5.jpg",
  // ];

  // var myAnimation = new hoverEffect({
  //   parent: document.querySelector(".my-div"),
  //   intensity: 0.3,
  //   image1: imgOne,
  //   image2: imgTwo,
  //   displacementImage: mydisplacementImage[type],
  // });

  return (
    <Image
      src={imgOne}
      width={600}
      height={500}
      alt="Picture of the author"
    />
  )
  // <div className="my-div bg-cover bg-center h-screen w-2/5"></div>;
}
