"use client";
import { useEffect, useState } from "react";
import { Dropdown, DropdownTrigger, DropdownMenu, DropdownSection, DropdownItem, Button } from "@nextui-org/react";
import { FaAngleDown } from "react-icons/fa";

interface MenuDropdownProps {
    selectedKeys: string;
    onSelectionChange: (keys: any) => any;
    titleOptions?: string;
    itemsDropdown: items[];
}

interface items {
    id: number;
    name: string;
}

const DropdownSelect = ({ selectedKeys, onSelectionChange, titleOptions, itemsDropdown }: MenuDropdownProps) => {

    const [selected, setSelected] = useState("")
    const [valueSelected, setValueSelected] = useState("")

    useEffect(() => {
        const ItemsDropdownSelected = itemsDropdown.find((item: { id: number }) => {
            return item.id == parseInt(selectedKeys.currentKey);
        }) 

        setSelected( ItemsDropdownSelected?.name ?? titleOptions );
        setValueSelected(ItemsDropdownSelected?.name ?? titleOptions);

    },[selectedKeys]);

    return {
        valueSelected,
        DropdownSelectProvider: (
            <div className="flex flex-col gap-2 justify-between relative">
                <label className="block z-10 pointer-events-none after:content-['*'] after:text-danger after:ml-0.5 will-change-auto origin-top-left transition-all !duration-200 !ease-[cubic-bezier(0,0,0.2,1)] motion-reduce:transition-none font-normal text-foreground-500 group-focus-within:font-medium group-[.is-filled]:font-medium group-focus-within:pointer-events-auto group-[.is-filled]:pointer-events-auto pb-0 group-focus-within:left-0 group-[.is-filled]:left-0 group-focus-within:text-foreground group-[.is-filled]:text-foreground text-small bottom-2.5 left-3 group-focus-within:bottom-12 group-[.is-filled]:bottom-12">{titleOptions}</label>
                <Dropdown className="border-1 border-default-200 rounded-lg">
                    <DropdownTrigger>
                        <Button
                            size="md" 
                            radius="sm" 
                            className="grow self-auto capitalize w-full justify-between"
                            endContent={<FaAngleDown />}
                        >
                            {selected}
                        </Button>
                    </DropdownTrigger>

                    <DropdownMenu
                        itemClasses={{
                            base: [
                                "rounded-md",
                                "text-default-500",
                                "transition-opacity",
                                "data-[pressed=true]:opacity-70",
                                "data-[focus-visible=true]:ring-default-500",
                                "px-4"
                            ],
                        }}
                        variant="flat"
                        disallowEmptySelection
                        selectionMode="single"
                        selectedKeys={selectedKeys}
                        onSelectionChange={onSelectionChange}
                    >
                        <DropdownSection className="w-[30rem] max-h-64 h-auto overflow-auto" title={titleOptions}>
                            {itemsDropdown.map((items: items) => (
                                <DropdownItem key={items.id}>{items.name}</DropdownItem>
                            ))}
                        </DropdownSection>
                    </DropdownMenu>
                </Dropdown>
            </div>
        )
    };
}

export default DropdownSelect;