"use client";
import Link from "next/link";
import { Dropdown, DropdownTrigger, DropdownMenu, DropdownSection, DropdownItem, User } from "@nextui-org/react";
import { ReactNode } from "react";

interface DropdownMenuProps {
  Trigger: ReactNode;
  DropdownItems: DropdownItemsProps;
  DropdownOptions: DropdownOptionsProps;
}

interface elementHtml {
  value: string
  | undefined
  | null
  | ReactNode;
}

interface DropdownItemsProps {
  [key: string]: items[];
}

interface items {
  name: string;
  icon?: elementHtml["value"];
  endContent: elementHtml["value"];
  handleClick?: () => void | null
  href?: string;
  color?: "default" | "primary" | "secondary" | "success" | "warning" | "danger" | undefined;
  shortcut?: string;
  className?: string | undefined;
  isReadOnly?: boolean;
  key?: string;
  description?: string;
}

interface DropdownOptionsProps {
  className?: string | undefined;
  itemClasses?: string[] | null | undefined;
  user?: boolean;
  logoutOption?: boolean;
}
type CollectionElement<T> = {
  key: string;
};

export default function MenuDropdown({
  Trigger,
  DropdownItems,
  DropdownOptions,
}: DropdownMenuProps) {
  return (
    <Dropdown className="border-1 border-default-200 rounded-lg">
      <DropdownTrigger>
        {Trigger}
      </DropdownTrigger>
      <DropdownMenu
        className={DropdownOptions.className}
        itemClasses={{
          base: [
            "rounded-md",
            "text-default-500",
            "transition-opacity",
            "data-[pressed=true]:opacity-70",
            "data-[focus-visible=true]:ring-default-500",
            "px-4",
            ...(DropdownOptions.itemClasses || []),
          ],
        }}
      >
        {renderDropdownUser(DropdownOptions)}

        {renderDropdownSection(DropdownItems, DropdownOptions.logoutOption)}

        {DropdownOptions.logoutOption && <DropdownSection >
          <DropdownItem key="logout" color="danger">Cerrar session</DropdownItem>
        </DropdownSection>}

      </DropdownMenu>
    </Dropdown>
  );
}

function renderDropdownUser({ user }: DropdownOptionsProps) {
  const userOptions: CollectionElement<DropdownOptionsProps>[] = user ? [{ key: "profile" }] : [];

  const showDivider = userOptions.length > 1;

  return (
    <DropdownSection showDivider={showDivider}>
      {userOptions.user && (
        <DropdownItem
          key={userOptions[0].key.toString()}
          className="h-14 gap-2 opacity-100"
        >
          <User
            name="Junior Garcia"
            description="@jrgarciadev"
            classNames={{
              name: "text-default-600",
              description: "text-default-500",
            }}
            avatarProps={{
              size: "sm",
              src: "https://avatars.githubusercontent.com/u/30373425?v=4",
            }}
          />
        </DropdownItem>
      )}
    </DropdownSection>
  );
}

function renderDropdownSection(itemsDropdown: DropdownItemsProps[], logoutOption: boolean) {
  if (!itemsDropdown) return null;
  const showDivider = logoutOption ? logoutOption : itemsDropdown.length > 1;

  return <DropdownSection showDivider={showDivider}>
    {Object.values(itemsDropdown).map((items) => {
      return items.map((item, index) => renderDropdownItem({ item, index }))
    })}
  </DropdownSection>;
}

function renderDropdownItem({ item, index}: { item: items, index: number }) {
  const startContent = item.icon;
  const content = item.href ? <Link href={item.href}>{item.name}</Link> : item.name;
  const shortcutContent = item.shortcut && item.shortcut.length > 0 ? item.shortcut : null;

  return <DropdownItem
    key={index}
    shortcut={shortcutContent}
    className={item.className}
    color={item.color}
    isReadOnly={item.isReadOnly}
    endContent={item.endContent}
    startContent={startContent}
    description={item.description}
    handleClick={item.handleClick}
  >
    {content}
  </DropdownItem>;
}
