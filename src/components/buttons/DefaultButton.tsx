"use client";
import { motion } from "framer-motion";
interface ButtonProps {
  icon?: string;
  text: string;
  onClick?: any;
  className?: string;
  type?: string;
}

export default function Button({
  icon,
  text,
  onClick,
  className,
  type,
}: ButtonProps) {
  return (
    <motion.button
      type={type}
      className={`${className} flex items-center justify-center gap-2 text-white 
    bg-black hover:bg-gray-800 focus:ring-4 focus:ring-gray-400 rounded-lg
    text-sm p-3 dark:bg-white dark:hover:bg-gray-200 focus:outline-none
    dark:focus:ring-gray-800 disable:bg-gray-400`}
      onClick={onClick}
      whileHover={{ scale: 1.02 }}
      whileTap={{ scale: 0.99 }}
      transition={{ type: "spring", stiffness: 400, damping: 10 }}
    >
      {icon ? icon : null}
      {text}
    </motion.button>
  );
}
