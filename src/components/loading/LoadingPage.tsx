"use client";
export default function LoadingPage(){
    return <div className="h-screen w-full flex justify-center items-center"><span className="loader"></span></div>
}