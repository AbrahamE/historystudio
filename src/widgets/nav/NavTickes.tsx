"use client";
import ButtonLink from "@/components/buttons/ButtonLink";

import { Avatar, AvatarGroup } from "@nextui-org/react";

import { FaBookmark, FaPlus } from "react-icons/fa";
import * as Progress from "@radix-ui/react-progress";
import * as HoverCard from '@radix-ui/react-hover-card';

interface NavTickesProps {
  progress: number | null;
}

export default function NavTickes({ progress }: NavTickesProps) {
  return (
    <section className="grow flex bg-white border-t border-gray-200 p-5">
      <section className="grow flex gap-2">
        <div className="grow flex items-center justify-between gap-2 w-1/3">
          <div className="grow flex items-center gap-2 w-1/3">
            <section className="inline-flex items-center justify-center w-10 h-10 overflow-hidden bg-gray-100 rounded-lg dark:bg-gray-600">
              <span className="font-medium text-gray-600 dark:text-gray-300">
                <FaBookmark />
              </span>
            </section>
            <section className="flex flex-col ">
              <p>Tickes Progrese</p>
              <Progress.Root
                className="relative overflow-hidden bg-gray-200 rounded-full w-[270px] h-[8px]"
                style={{
                  transform: "translateZ(0)",
                }}
                value={progress}
              >
                <Progress.Indicator
                  className="bg-black w-full h-full transition-transform duration-[660ms] ease-[cubic-bezier(0.65, 0, 0.35, 1)]"
                  style={{
                    transform: `translateX(-${100 - (progress ?? 0)}%)`,
                  }}
                />
              </Progress.Root>
            </section>
            <span>{progress}%</span>
          </div>
        </div>

        <div className="flex items-center justify-between gap-2">
          <section className="flex -space-x-4">

            <AvatarGroup isBordered>
            <HoverCard.Root>
                <HoverCard.Trigger asChild>
                  <Avatar src="https://i.pravatar.cc/150?u=a04258114e29026708c" />
                </HoverCard.Trigger>
                <HoverCard.Portal>
                  <HoverCard.Content
                    className="data-[side=bottom]:animate-slideUpAndFade 
                    data-[side=right]:animate-slideLeftAndFade 
                    data-[side=left]:animate-slideRightAndFade 
                    data-[side=top]:animate-slideDownAndFade 
                    w-[300px] border-1 border-default-200 rounded-lg bg-white p-5 shadow-3xl data-[state=open]:transition-all"
                    sideOffset={5}
                  >
                    <div className="flex flex-col gap-[7px]">
                      <img
                        className="block h-[60px] w-[60px] rounded-full"
                        src="https://i.pravatar.cc/150?u=a04258114e29026708c"
                        alt="Radix UI"
                      />
                      <div className="flex flex-col gap-[15px]">
                        <div>
                          <div className="text-mauve12 m-0 text-[15px] font-medium leading-[1.5]">Radix</div>
                          <div className="text-mauve10 m-0 text-[15px] leading-[1.5]">@radix_ui</div>
                        </div>
                        <div className="text-mauve12 m-0 text-[15px] leading-[1.5]">
                          Components, icons, colors, and templates for building high-quality, accessible UI.
                          Free and open-source.
                        </div>
                        <div className="flex gap-[15px]">
                          <div className="flex gap-[5px]">
                            <div className="text-mauve12 m-0 text-[15px] font-medium leading-[1.5]">0</div>{' '}
                            <div className="text-mauve10 m-0 text-[15px] leading-[1.5]">Following</div>
                          </div>
                          <div className="flex gap-[5px]">
                            <div className="text-mauve12 m-0 text-[15px] font-medium leading-[1.5]">2,900</div>{' '}
                            <div className="text-mauve10 m-0 text-[15px] leading-[1.5]">Followers</div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <HoverCard.Arrow className="fill-white" />
                  </HoverCard.Content>
                </HoverCard.Portal>
              </HoverCard.Root>
              <Avatar src="https://i.pravatar.cc/150?u=a042581f4e29026024d" />
              <Avatar src="https://i.pravatar.cc/150?u=a04258a2462d826712d" />
              <Avatar src="https://i.pravatar.cc/150?u=a042581f4e29026704d" />
              <Avatar src="https://i.pravatar.cc/150?u=a04258114e29026302d" />
              <Avatar src="https://i.pravatar.cc/150?u=a04258114e29026702d" />
            </AvatarGroup>
          </section>
          <section>
            <ButtonLink
              className="flex items-center justify-center gap-2 text-white bg-black hover:bg-gray-800 focus:ring-4 focus:ring-gray-400 rounded-lg text-sm p-3 dark:bg-white dark:hover:bg-gray-200 focus:outline-none dark:focus:ring-gray-800"
              href="/dashboard/tickets/create/quotation"
              icon={<FaPlus />}
              text="Crear nueva cotización"
            />
          </section>
        </div>
      </section>
    </section>
  );
}
