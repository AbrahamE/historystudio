"use client";
import * as Tabs from "@radix-ui/react-tabs";
import React, {
    ReactElement,
    JSXElementConstructor,
    ReactFragment,
    ReactPortal,
    PromiseLikeOfReactNode,
} from "react";


interface TabsProps {
    TabsList: {
        tab: string;
        className?: string | null;
    }[];
    TabsSection?: {
        section?: string
        | ReactElement<any, string
        | JSXElementConstructor<any>>
        | ReactFragment
        | ReactPortal
        | PromiseLikeOfReactNode
        | React.Component<any, any>
        | React.ReactNode
        | null
        | undefined;
        className?: string | null;
    };
    TabsContent: {
        content?: string
        | ReactElement<any, string
        | JSXElementConstructor<any>>
        | ReactFragment
        | ReactPortal
        | PromiseLikeOfReactNode
        | React.Component<any, any>
        | React.ReactNode
        | null
        | undefined;
        contentName: string;
        className?: string | null;
    }[];
    TabsRoot: {
        className?: string | null;
    };
    defaultValue: string;
}
/**
 * Renders a set of tabs with customizable sections and content.
 *
 * @param {Object} props - The props object containing the following properties:
 *   - {Array} TabsList: An array of tab objects with `tab` and `className` properties.
 *   - {Object} TabsSection: An optional object representing the section of the tabs with `section` and `className` properties.
 *   - {Array} TabsContent: An array of content objects with `content`, `contentName`, and `className` properties.
 * @return {JSX.Element} The rendered tabs component.
*/
export default function TabsSection({
    TabsList,
    TabsSection,
    TabsContent,
    TabsRoot,
    defaultValue
}: TabsProps): JSX.Element {

    return <>
        {(TabsList.length) &&
            <Tabs.Root className={`flex flex-col w-full ${TabsRoot.className}`} defaultValue={defaultValue}>
                <Tabs.List className="shrink-0 flex w-full border-b border-gray-200">
                    {TabsList.map(({ tab, className }) => (
                        <Tabs.Trigger
                            className={`bg-white px-5 h-[45px] flex grow-none items-center justify-center text-[15px] hover:text-violet11 data-[state=active]:text-violet11 data-[state=active]:shadow-[inset_0_-1px_0_0,0_1px_0_0] data-[state=active]:shadow-current data-[state=active]:focus:relative data-[state=active]:focus:shadow-black cursor-default ${className}`}
                            value={tab}
                        >
                            {tab}
                        </Tabs.Trigger>
                    ))}
                    {TabsSection && (
                        <section className={`bg-white grow flex justify-end ${TabsSection.className}`}>
                            {TabsSection.section}
                        </section>
                    )}
                </Tabs.List>

                {TabsContent.map(({ content, contentName, className }) => (
                    <Tabs.Content className={`grow p-5 ${className}`} value={contentName}>
                        {content}
                    </Tabs.Content>
                ))}
            </Tabs.Root>}
    </>

}