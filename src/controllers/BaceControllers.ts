"use client";
import { createClient } from "@supabase/supabase-js";
import { useStore } from "@/store/Tattoo/tattooResources";
import { createSelectors } from "@/store/index";

const supabaseUrl = "https://dgkazwgixzrmynaxmacn.supabase.co";
const supabaseKey =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImRna2F6d2dpeHpybXluYXhtYWNuIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODM0NzQ2MjQsImV4cCI6MTk5OTA1MDYyNH0.0AuwQNDHubzq1LhhabNxPXEeVDFsRQeV3FRVr4u4Qhg";
const supabase = createClient(supabaseUrl, supabaseKey);

export default function BaceControllers() {
  const resources = createSelectors(useStore);

  return {
    client: supabase,
    getValueSet: (Set: any) => {
      return [Set[Symbol.iterator]().next().value, Set]
    },
    storeApp: {
      storeArtist: resources.use.getArtist() ?? [],
      storeTypePaiment: resources.use.getTypePaiment() ?? []
    },
  };
}
