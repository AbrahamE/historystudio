"use client";
import { ReactNode } from "react"
import FormLayout from "@/components/form/FormLayout";

export default function Layout(props: { login: ReactNode, children: ReactNode;}) {
  return (
    <div className="flex w-full h-screen">
         {props.children}
      <FormLayout>
        <div className="flex flex-col justify-center items-center gap-2 grow m-16 p-10 z-10">
          {props.login}
        </div>
      </FormLayout>
    </div>
  );
}
