"use client";
import { Input } from "@nextui-org/react";
import {
  SetStateAction,
  useState,
  Dispatch,
} from "react";

interface InputProps {
  type: string;
  name: string;
  label?: string;
  value: Dispatch<SetStateAction<string>>;
  className?: string[];
  placeholder?: string;
  defaultValue?: string;
  labelPlacement?: "outside" | "outside-left" | "inside" | undefined;
  validationState: "valid" | "invalid";
  isClearable?: boolean;
  isRequired?: boolean;
  isReadOnly?: boolean;
  isDisabled?: boolean;
  errorMessage?: string;
  startContent?: React.ReactNode;
  endContent?: React.ReactNode;
}

export default function InputDefault({
  label,
  name,
  type,
  value,
  className,
  placeholder,
  defaultValue,
  labelPlacement,
  validationState,
  isClearable,
  isRequired,
  isReadOnly,
  isDisabled,
  errorMessage,
  startContent,
  endContent,
}: InputProps) {
  const [inputValue, setInputValue] = useState("");

  function handleInputChange(event: {
    target: { value: SetStateAction<string> };
  }) {
    setInputValue(event.target.value);
    value(event.target.value);
  }

  function handleClear() {
    if(isClearable){
      setInputValue("");
      value("");
    }
  }

  return (
    <>
      <Input
        type={type}
        label={label}
        value={inputValue}
        name={name}
        defaultValue={defaultValue}
        placeholder={placeholder}
        labelPlacement={labelPlacement ?? "outside"}
        validationState={validationState}
        size="md" 
        radius="sm" 
        className={{
          input: [
            "bg-white",
            "rounded-lg",
            ...(className || [])
          ],
          innerWrapper: ["bg-transparent",  "rounded-lg"],
          inputWrapper: [
            "bg-white",
            "rounded-lg",
          ]
        }}
        onClear={handleClear}
        onChange={handleInputChange}
        isClearable={isClearable}
        isRequired={isRequired}
        isReadOnly={isReadOnly}
        isDisabled={isDisabled}
        errorMessage={errorMessage}
        startContent={startContent}
        endContent={endContent}
      />
    </>
  );
}
