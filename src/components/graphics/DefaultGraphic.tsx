"use client";
import ReactECharts from "echarts-for-react";

interface DefaultGraphicProps {
  width: number;
  height: number;
  option: Object;
}

export default function DefaultGraphic({
  width,
  height,
  option,
}: DefaultGraphicProps) {
  return (
    <div className="bg-white p-5 rounded-lg">
      <ReactECharts style={{ width, height }} option={option} />
    </div>
  );
}
