import BaceControllers from "@/controllers/BaceControllers";

export function TattooControllers() {
  const { client } = BaceControllers();

  async function getBodyLocations() {
    return client.from("body_location").select();
  }

  async function getTypesTattoo() {
    return client.from("types_tattoo").select();
  }

  async function getStylesTattoo() {
    return client.from("styles_tattoo").select();
  }

  async function getArtist() {
    return client.from("artist").select();
  }

  async function getTypePaiment() {
    return client.from("type_paiment").select();
  }

  return {
    getBodyLocations,
    getTypesTattoo,
    getStylesTattoo,
    getArtist,
    getTypePaiment
  };
}
