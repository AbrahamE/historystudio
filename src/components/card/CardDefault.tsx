"use client";
import { ReactNode } from "react";
import { Card, CardHeader, CardBody, CardFooter, Divider, Link, Image } from "@nextui-org/react";

interface CardPricesProps {
  header?: ReactNode;
  children?: ReactNode;
  footer?: ReactNode;
  className?: string;
}

export default function CardDefault({ header, children, footer, className }: CardPricesProps) {
  return (
    <Card className={`max-w-screen border-1 border-default-200 rounded-lg ${className}`}>
      <CardHeader className="flex px-16 py-5 items-center">
        {header}
      </CardHeader>
      <Divider />
      <CardBody className="p-16">
        {children}
      </CardBody>
      <Divider />
      <CardFooter className="flex px-16 py-5 justify-end items-center">
        {footer}
      </CardFooter>
    </Card>
  );
}
