import BaceControllers from "@/controllers/BaceControllers";
import { AuthUser } from "@/types/User/AuthUser";

export function AuthControllers() {
  const { client } = BaceControllers();

  async function login({ email, password }: AuthUser) {
    return client.auth.signInWithPassword({ email, password });
  }

  async function logout() {
    return client.auth.signOut();
  }

  async function getSession() {
    return client.auth.getSession();
  }

  return {
    login,
    logout,
    getSession,
  };
}
