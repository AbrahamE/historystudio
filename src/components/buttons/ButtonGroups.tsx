"use client";
import React from 'react';
import * as ToggleGroup from '@radix-ui/react-toggle-group';
import { TextAlignLeftIcon, TextAlignCenterIcon, TextAlignRightIcon } from '@radix-ui/react-icons';

interface ButtonGroupsProps {
  groups?: Groups[]
}

interface Groups {
  value: string;
  icon?: React.ReactNode;
  handleClick?: () => void;
}


const toggleGroupItemClasses =
  'hover:bg-gray-800 hover:text-white color-black data-[state=on]:bg-gray-800 data-[state=on]:text-white flex h-[35px] w-[35px] items-center justify-center bg-white text-base leading-4 first:rounded-l-lg last:rounded-r-lg focus:z-10 shadow focus:shadow-black focus:outline-none';

export default function ButtonGroups({ groups }: ButtonGroupsProps) {
  return <ToggleGroup.Root
    className="flex bg-white rounded justify-center items-center px-5 m-0 space-x-px space-y-px"
    type="single"
    defaultValue="center"
    aria-label="Text alignment"
  >

    {groups ? groups.map((item: Groups) => {
      return <ToggleGroup.Item
        className={toggleGroupItemClasses}
        value={item.value}
        aria-label={item.value + " aligned"}
        onClick={item.handleClick}
      >
        {item.icon}
      </ToggleGroup.Item>
    }) : null}
  </ToggleGroup.Root>
}

