"use client";
import { useState, useEffect } from "react";
import * as Toast from '@radix-ui/react-toast';
import { Button } from "@nextui-org/react";

interface ToastControlProps {
    initialTitle: string;
    initialDescription: string;
    initialAction?: {
        title: string;
        handlerClick?: () => void | null | undefined;
    } | null;
    initialTimeout: number;
}

const useToastControl = ({ initialTitle, initialDescription, initialTimeout, initialAction }: ToastControlProps) => {

    const [open, setOpen] = useState(false);
    const [title, setTitle] = useState(initialTitle);
    const [description, setDescription] = useState(initialDescription);

    useEffect(() => {
        let timer: NodeJS.Timeout;

        if (open) {
            timer = setTimeout(() => {
                setOpen(false);
            }, initialTimeout);
        }

        return () => {
            clearTimeout(timer);
        };
    }, [open, initialTimeout]);

    const closeToast = () => {
        setOpen(false);
    };

    const openToast = () => {
        setOpen(true);
    };

    const updateDescription = (newDescription: string) => {
        setDescription(newDescription);
        openToast()
    };

    const updateTitle = (newtitle: string) => {
        setTitle(newtitle);
    };

    return {
        description,
        closeToast,
        openToast,
        ToastProvider: (
            <Toast.Provider swipeDirection="right">
                <Toast.Root
                    className="bg-white rounded-md shadow-3xl p-[15px] grid [grid-template-areas:_'title_action'_'description_action'] grid-cols-[auto_max-content] gap-x-[15px] items-center data-[state=open]:animate-slideIn data-[state=closed]:animate-hide data-[swipe=move]:translate-x-[var(--radix-toast-swipe-move-x)] data-[swipe=cancel]:translate-x-0 data-[swipe=cancel]:transition-[transform_200ms_ease-out] data-[swipe=end]:animate-swipeOut"
                    open={open}
                    onOpenChange={setOpen}>
                    <Toast.Title className="[grid-area:_title] mb-[5px] font-medium text-slate12 text-[15px]">
                        {title}
                    </Toast.Title>
                    <Toast.Description asChild>
                        <p className="[grid-area:_description] m-0 text-slate11 text-[13px] leading-[1.3]">{description}</p>
                    </Toast.Description>
                    <Toast.Action className="[grid-area:_action]" asChild altText="Goto schedule to undo">
                        {initialAction && (
                            <Button className="rounded-lg" color="success" onClick={initialAction.handlerClick}>
                                {initialAction.title}
                            </Button>
                        )}
                    </Toast.Action>
                    <Toast.Close />
                </Toast.Root>
                <Toast.Viewport className="[--viewport-padding:_25px] fixed top-0 right-0 flex flex-col p-[var(--viewport-padding)] gap-[10px] w-[390px] max-w-[100vw] m-0 list-none z-[2147483647] outline-none" />
            </Toast.Provider>
        ),
        updateDescription,
        updateTitle,
    };
}

export default useToastControl;