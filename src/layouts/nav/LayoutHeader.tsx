"use client";
import { motion } from "framer-motion";
import getScrollingPosition from "@/hook/getScrolling";

const PX_DEFAULT = "0px";

interface headerProps {
  children: React.ReactNode;
  className?: string;
  disengaged?: boolean;
}

const Header: React.FC<headerProps> = ({ children, className, disengaged }) => {
  const [scroll] = getScrollingPosition();
  let scrollUp = (scroll == "up"); 
  let margin = disengaged ? (!scrollUp) ? PX_DEFAULT : "1.25rem 2.5rem" : null


  return (
    <motion.header
      animate={{
        backgroundColor: scrollUp ? "#fff" : "rgb(255 255 255 / 0.5)",
        position: scrollUp ? "relative" : "fixed",
        borderRadius: !scrollUp ? PX_DEFAULT : "",
        margin
      }}
      className={`z-30 ${className}`}
      aria-label="Breadcrumb"
    >
      {children}
    </motion.header>
  );
};

export default Header;
