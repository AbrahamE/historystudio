"use client";
import { useState } from "react";
import { usePathname } from "next/navigation";
import { default as Link } from "@/components/buttons/ButtonLink";
import Header from "@/layouts/nav/LayoutHeader";

import {
  getUrlArrayPathNameSlice,
  getUrlArrayPathName,
} from "@/hook/getUrlArrayPathName";

function BreadcrumbsIcon(pathAction: string[], path: string) {
  return (
    <>
      {pathAction.includes(path) ? null : (
        <svg
          aria-hidden="true"
          className="w-6 h-6 text-gray-400"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill-rule="evenodd"
            d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
            clip-rule="evenodd"
            style={{ fill: "currentColor" }}
          ></path>
        </svg>
      )}
    </>
  );
}

export default function Breadcrumbs() {
  const pathName: string | null = usePathname();
  const [urlPath, serUrlPath] = useState(getUrlArrayPathNameSlice(pathName));
  const [urlPathName, serUrlPathName] = useState(getUrlArrayPathName(pathName));
  const pathAction = ["create", "list"];

  return (
    <Header
      className="my-5 mx-10 top-0 justify-between px-5 py-4 text-gray-700 border 
        border-gray-200 rounded-lg sm:flex sm:px-5 dark:bg-gray-800 
        dark:border-gray-700 backdrop-blur-sm bg-white
        transition max-w-[100%] w-full ease-in-out delay-150"
      disengaged={true}
    >
      <ol className="inline-flex items-center mb-3 sm:mb-0">
        {urlPath.map((path: string, index: string | number) => {
          return (
            <li className="m-0" key={index}>
              <div className="flex justify-between items-center">
                <Link
                  className="ml-1 text-sm font-medium 
                    text-gray-700 hover:text-black md:ml-2 
                    dark:text-gray-400 dark:hover:text-white"
                  href={urlPath[index]}
                  text={urlPathName[index]}
                />

                {BreadcrumbsIcon(pathAction, path)}
              </div>
            </li>
          );
        })}
      </ol>
    </Header>
  );
}
